<?php

namespace App\Http\Controllers\RentAd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RentAdController extends Controller
{
    public function index()
    {
        return view('frontend.rent_ad.home');
    }

    public function faq()
    {
        return view('frontend.rent_ad.faq');
    }

    public function terms_conditions()
    {
        return view('frontend.rent_ad.terms_conditions');
    }

    public function about_us()
    {
        return view('frontend.rent_ad.about_us');
    }
}
