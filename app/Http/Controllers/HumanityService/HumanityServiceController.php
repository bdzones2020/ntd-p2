<?php

namespace App\Http\Controllers\HumanityService;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HumanityServiceController extends Controller
{
    public function index()
    {
        return view('frontend.humanity_service.home');
    }

    public function faq()
    {
        return view('frontend.humanity_service.faq');
    }

    public function terms_conditions()
    {
        return view('frontend.humanity_service.terms_conditions');
    }

    public function about_us()
    {
        return view('frontend.humanity_service.about_us');
    }
}
