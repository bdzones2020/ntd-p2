<?php

namespace App\Http\Controllers\HumanResource;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HumanResourceController extends Controller
{
    public function index()
    {
        return view('frontend.human_resource.home');
    }

    public function faq()
    {
        return view('frontend.human_resource.faq');
    }

    public function terms_conditions()
    {
        return view('frontend.human_resource.terms_conditions');
    }

    public function about_us()
    {
        return view('frontend.human_resource.about_us');
    }
}
