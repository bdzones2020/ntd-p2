<?php

namespace App\Http\Controllers\SaleInfo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SaleInfoController extends Controller
{
    public function index()
    {
        return view('frontend.sale_info.home');
    }

    public function faq()
    {
        return view('frontend.sale_info.faq');
    }

    public function terms_conditions()
    {
        return view('frontend.sale_info.terms_conditions');
    }

    public function about_us()
    {
        return view('frontend.sale_info.about_us');
    }
}
