<?php

namespace App\Http\Controllers\LostFound;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\LostFound\LostFound;
use App\Models\LostFound\LostFoundImage;
use App\Models\LostFound\LostFoundResponse;
use App\Models\LostFound\LostFoundResponseImage;
use Illuminate\Support\Facades\Auth;

use App\Helpers\ImageHelper;

class LostFoundController extends Controller
{
    public $imageHelper;

    public function __construct(ImageHelper $imageHelper)
    {
        $this->imageHelper = $imageHelper;
    }

    public function data_list()
    {
        $lost_founds = LostFound::with(['division','district','thana'])->where('is_archived',null)->orderBy('occurance_date','DESC')->paginate(10);
        return response()->json($lost_founds);
    }

    public function index()
    {
        $lost_founds = LostFound::select('id','user_id','lost_found_code','title','occurance_type','occurance_date','subject_type','subject_name','tags','division_id','district_id','thana_id','address','created_at')->with(['user','division','district','thana','first_image'])->where('is_archived',null)->orderBy('id','DESC')->paginate(30);
        return view('frontend.lost_found.home',compact('lost_founds'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'occurance_type' => 'required|string',
            'subject_type' => 'required|string',
            'subject_name' => 'required|string',
            'division_id' => 'required',
            'district_id' => 'required',
            'thana_id' => 'required',
            'address' => 'required',
            'occurance_date' => 'required',
            'hour' => 'nullable',
            'minute' => 'nullable',
            'period' => 'nullable',
            'description' => 'required',
            'tags' => 'nullable',
            'duration' => 'required',
        ]);

        //return $request;
        // substr($lost_found->title, 0, 25)
        // strlen($lost_found->title)

        $lost_found = new LostFound;
        $lost_found->user_id = Auth::check() ? Auth::user()->id : null ;
        $lost_found->title = $request->title;
        $lost_found->occurance_type = $request->occurance_type;
        $lost_found->subject_type = $request->subject_type;
        $lost_found->subject_name = $request->subject_name;
        $lost_found->division_id = $request->division_id;
        $lost_found->district_id = $request->district_id;
        $lost_found->thana_id = $request->thana_id;
        $lost_found->address = $request->address;
        $lost_found->occurance_date = date('Y-m-d', strtotime($request->occurance_date));
        $lost_found->occurance_time = $request->period.' '.$request->hour.' '.$request->minute;
        $lost_found->description = $request->description;
        $lost_found->tags = $request->tags;
        $lost_found->duration = $request->duration;
        $lost_found->status = 'pending';
        $lost_found->save();

        if (count($request->uploadedImages) > 0) {

            foreach ($request->uploadedImages as $k => $uploadedImage) {

                $lost_found_image = new LostFoundImage;
                $lost_found_image->lost_found_id = $lost_found->id;
                //$lost_found_image->caption = $request->caption[$k] ? $request->caption[$k] : null;

                //$encodeType = explode('/', explode(';', $uploadedImage)[0])[1];

                // 1 mb = 1048576 bytes in binary which is countable for the image size here
                $image_name = $this->imageHelper->uploadImage(null, $request->images[$k], 'lost_founds', null, 5248576, null, 600, $lost_found->subject_name.$k, ['width' => null, 'height' => 200, 'thumbStorageName' => 'lost_found_thumbs'] );
                // get max size alert
                if($image_name == 'MaxSizeErr') {
                    return back()->with('message_warning', 'Too large file (max limit:5mb)');
                }

                $lost_found_image->image_link =  $image_name;
                $lost_found_image->save();
            }
        }

        return back()->with('success','আপনার প্রকাশ / পোষ্ট সফলভাবে তালিকায় যুক্ত করা হয়েছে।');
    }

    public function details($id)
    {
        $lost_found = LostFound::with(['user','division','district','thana','lost_found_images','lost_found_responses.user'])->find($id);
        // return $lost_found->lost_found_responses;
        return view('frontend.lost_found.details',compact('lost_found'));
    }

    public function store_response(Request $request)
    {
        $this->validate($request, [
            'lost_found_id' => 'required|integer',
            'division_id' => 'required',
            'district_id' => 'required',
            'thana_id' => 'required',
            'address' => 'required',
            'description' => 'required'
        ]);

        $lost_found_response = new LostFoundResponse;
        $lost_found_response->lost_found_id = $request->lost_found_id;
        $lost_found_response->user_id = Auth::check() ? Auth::user()->id : null ;
        $lost_found_response->description = $request->description;
        $lost_found_response->save();

        if (count($request->uploadedImages) > 0) {

            foreach ($request->uploadedImages as $k => $uploadedImage) {

                $lost_found_response_image = new LostFoundResponseImage;
                $lost_found_response_image->lost_found_id = $lost_found_response->lost_found_id;
                $lost_found_response_image->lost_found_response_id = $lost_found_response->id;
                //$lost_found_image->caption = $request->caption[$k] ? $request->caption[$k] : null;

                //$encodeType = explode('/', explode(';', $uploadedImage)[0])[1];

                // 1 mb = 1048576 bytes in binary which is countable for the image size here
                $image_name = $this->imageHelper->uploadImage(null, $request->responseImages[$k], 'lost_founds', null, 5248576, null, 600, $lost_found_response->lost_found_id.'_response_'.$k, ['width' => null, 'height' => 200, 'thumbStorageName' => 'lost_found_thumbs'] );
                // get max size alert
                if($image_name == 'MaxSizeErr') {
                    return back()->with('message_warning', 'Too large file (max limit:5mb)');
                }

                $lost_found_response_image->image_link =  $image_name;
                $lost_found_response_image->save();
            }
        }

        return back()->with('success','আপনার সাড়া / প্রতিউত্তর সফলভাবে যুক্ত করা হয়েছে।');
    }

    public function faq()
    {
        return view('frontend.lost_found.faq');
    }

    public function terms_conditions()
    {
        return view('frontend.lost_found.terms_conditions');
    }

    public function about_us()
    {
        return view('frontend.lost_found.about_us');
    }
}
