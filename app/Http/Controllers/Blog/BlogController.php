<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        return view('frontend.blog.home');
    }

    public function faq()
    {
        return view('frontend.blog.faq');
    }

    public function terms_conditions()
    {
        return view('frontend.blog.terms_conditions');
    }

    public function about_us()
    {
        return view('frontend.blog.about_us');
    }
}
