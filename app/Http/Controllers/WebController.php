<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index()
    {
        return view('frontend.web.home');
    }

    public function faq()
    {
        return view('frontend.web.faq');
    }

    public function terms_conditions()
    {
        return view('frontend.web.terms_conditions');
    }

    public function about_us()
    {
        return view('frontend.web.about_us');
    }
}
