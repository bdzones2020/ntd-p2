<?php

namespace App\Http\Controllers\Exchange;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExchangeController extends Controller
{
    public function index()
    {
        return view('frontend.exchange.home');
    }

    public function faq()
    {
        return view('frontend.exchange.faq');
    }

    public function terms_conditions()
    {
        return view('frontend.exchange.terms_conditions');
    }

    public function about_us()
    {
        return view('frontend.exchange.about_us');
    }
}
