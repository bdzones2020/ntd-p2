<?php

namespace App\Http\Controllers\Tsc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TscController extends Controller
{
    public function index()
    {
        return view('frontend.tsc.home');
    }

    public function faq()
    {
        return view('frontend.tsc.faq');
    }

    public function terms_conditions()
    {
        return view('frontend.tsc.terms_conditions');
    }

    public function about_us()
    {
        return view('frontend.tsc.about_us');
    }
}
