<?php

namespace App\Http\Controllers\Information;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    public function index()
    {
        return view('frontend.information.home');
    }

    public function faq()
    {
        return view('frontend.information.faq');
    }

    public function terms_conditions()
    {
        return view('frontend.information.terms_conditions');
    }

    public function about_us()
    {
        return view('frontend.information.about_us');
    }
}
