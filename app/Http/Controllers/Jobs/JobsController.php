<?php

namespace App\Http\Controllers\Jobs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JobsController extends Controller
{
    public function index()
    {
        return view('frontend.jobs.home');
    }

    public function faq()
    {
        return view('frontend.jobs.faq');
    }

    public function terms_conditions()
    {
        return view('frontend.jobs.terms_conditions');
    }

    public function about_us()
    {
        return view('frontend.jobs.about_us');
    }
}
