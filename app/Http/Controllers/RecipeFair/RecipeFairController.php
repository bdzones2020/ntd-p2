<?php

namespace App\Http\Controllers\RecipeFair;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RecipeFairController extends Controller
{
    public function index()
    {
        return view('frontend.recipe_fair.home');
    }

    public function faq()
    {
        return view('frontend.recipe_fair.faq');
    }

    public function terms_conditions()
    {
        return view('frontend.recipe_fair.terms_conditions');
    }

    public function about_us()
    {
        return view('frontend.recipe_fair.about_us');
    }
}
