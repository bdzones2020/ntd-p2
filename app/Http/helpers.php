<?php

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

if (!function_exists('appInfo')) {
    function appInfo()
	{
		$jsonString = file_get_contents(config_path('appInfo.json'));
		return json_decode($jsonString, true);
	}
}

if (!function_exists('divisions')) {
    function divisions()
    {
        return DB::connection('locations')->table('divisions')->get();
    }
}

if (!function_exists('districts')) {
    function districts()
    {
        return DB::connection('locations')->table('districts')->get();
    }
}

if (!function_exists('thanas')) {
    function thanas()
    {
        return DB::connection('locations')->table('thanas')->get();
    }
}

if (!function_exists('get_subject')) {
    function get_subject($name)
    {
        $get_name = '';
        if($name == 'pet')
        {
            $get_name = 'পোষা প্রাণী';
        }elseif($name == 'person')
        {
            $get_name = 'ব্যক্তি';
        }elseif($name == 'meterial')
        {
            $get_name = 'বস্তু';
        }

        return $get_name;
    }
}