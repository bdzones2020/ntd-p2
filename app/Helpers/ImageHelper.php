<?php

namespace App\Helpers;

use Str;
use Image;
use Storage;

class ImageHelper
{
    public function uploadImage($encodeType = null, $image, $storageName, $checkImage = null, $maxSize = null, $width = null, $height = null, $imageName = null, $thumbnail = ['width' => null, 'height' => null, 'thumbStorageName' => null])
    {
        if ($image) {
            // check image size
            if(!$encodeType)
            {
                $imageSize = $image->getSize();
                // 1 mb = 1048576 bytes in binary which is countable for the image size here
                if($maxSize !== 0 && $maxSize !== null){
                    if ($imageSize > $maxSize) {
                        return 'MaxSizeErr';
                    }
                }
            }
            // check existing image
            if($checkImage != null) {
                if (Storage::disk($storageName)->has($checkImage)) {
                    Storage::disk($storageName)->delete($checkImage);
                }
            }
            // rename image
            if($imageName == null) {
                $imageName = $encodeType ? date('Y_m_d_h_i_s') . '.' .$encodeType : date('Y_m_d_h_i_s') . '.' . $image->getClientOriginalExtension();
            }else{
                $imageName = $encodeType ? Str::slug($imageName, '_') . date('Y_m_d_h_i_s') . '.' .$encodeType : Str::slug($imageName, '_') . date('Y_m_d_h_i_s') . '.' . $image->getClientOriginalExtension();
            }
            // get the image
            $imageFile = $encodeType ? Image::make($image)->encode($encodeType) : Image::make($image);
            // resize image when width & height are null
            if($width === null && $height === null) {
                $imageFile->stream();
            }
            // resize image when width & height are not null
            if($width != null && $height != null) {
                $imageFile->resize($width, $height)->stream();
            }
            // resize image when width is not null & height is null
            if($width != null && $height === null) {
                $imageFile->resize($width, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->stream();
            }
            // resize image when width is null & height is not null
            if($width === null && $height != null) {
                $imageFile->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                })->stream();
            }
            // resize image when width is not null & height is 0 means no change of height
            if($width != null && $height === 0) {
                $imageFile->resize($width, null)->stream();
            }
            // resize image when width is 0 means no change & height is not null
            if($width === 0 && $height != null) {
                $imageFile->resize(null, $height)->stream();
            }
            // upload or store image in the particular storage directory
            $uploadedFile = Storage::disk($storageName)->put($imageName, $imageFile);

            $imageFile->destroy(); // free the memory that intervention was using
            unset($uploadedFile); // same here since it contains the entire jpg as a string


            // if thumbnail request

            if($thumbnail['thumbStorageName'] != null)
            {
                // check existing image
                if($checkImage != null) {
                    if (Storage::disk($thumbnail['thumbStorageName'])->has($checkImage)) {
                        Storage::disk($thumbnail['thumbStorageName'])->delete($checkImage);
                    }
                }
                // get the image
                $thumbFile = $encodeType ? Image::make($image)->encode($encodeType) : Image::make($image);
                // resize image when width & height are null
                if($thumbnail['width'] === null && $thumbnail['height'] === null) {
                    $thumbFile->stream();
                }
                // resize image when width & height are not null
                if($thumbnail['width'] != null && $thumbnail['height'] != null) {
                    $thumbFile->resize($thumbnail['width'], $thumbnail['height'])->stream();
                }
                // resize image when width is not null & height is null
                if($thumbnail['width'] != null && $thumbnail['height'] === null) {
                    $thumbFile->resize($thumbnail['width'], null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->stream();
                }
                // resize image when width is null & height is not null
                if($thumbnail['width'] === null && $thumbnail['height'] != null) {
                    $thumbFile->resize(null, $thumbnail['height'], function ($constraint) {
                        $constraint->aspectRatio();
                    })->stream();
                }
                // resize image when width is not null & height is 0 means no change of height
                if($thumbnail['width'] != null && $thumbnail['height'] === 0) {
                    $thumbFile->resize($thumbnail['width'], null)->stream();
                }
                // resize image when width is 0 means no change & height is not null
                if($thumbnail['width'] === 0 && $thumbnail['height'] != null) {
                    $thumbFile->resize(null, $thumbnail['height'])->stream();
                }
                // upload or store image in the particular storage directory
                $uploadedFile = Storage::disk($thumbnail['thumbStorageName'])->put($imageName, $thumbFile);

                $thumbFile->destroy(); // free the memory that intervention was using
                unset($uploadedFile); // same here since it contains the entire jpg as a string

            }

            return $imageName;

        }else{
            return 'No image found';
        }
    }
}