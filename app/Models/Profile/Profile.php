<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'full_name', 'father_name', 'mother_name', 'spouse_name', 'dob', 'gender', 'religion', 'photo', 'present_division_id', 'present_district_id', 'present_thana_id', 'present_postal_code', 'present_address', 'permanent_division_id', 'permanent_district_id', 'permanent_thana_id', 'permanent_postal_code', 'permanent_address', 'personal_contact_numbers', 'personal_contact_email', 'emergency_contact_person', 'emergency_contact_relation', 'emergency_contact_number', 'profile_score', 'referral_code'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
