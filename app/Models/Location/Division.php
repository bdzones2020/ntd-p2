<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Location\District;

class Division extends Model
{
    use HasFactory;

    protected $connection = 'locations';
    protected $table = 'divisions';

    public function districts()
    {
        return $this->hasMany(District::class)->select('id','division_id','name','slug');
    }
}
