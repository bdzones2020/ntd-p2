<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Location\Thana;

class District extends Model
{
    use HasFactory;

    protected $connection = 'locations';
    protected $table = 'districts';

    public function thanas()
    {
        return $this->hasMany(Thana::class)->select('id','district_id','name','slug');
    }
}
