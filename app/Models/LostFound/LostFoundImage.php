<?php

namespace App\Models\LostFound;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LostFoundImage extends Model
{
    use HasFactory;

    protected $connection = 'ntd_lost_found';
    protected $table = 'lost_found_images';

    protected $fillable = ['lost_found_id','image_link','caption'];
}
