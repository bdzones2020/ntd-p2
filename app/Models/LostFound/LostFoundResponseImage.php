<?php

namespace App\Models\LostFound;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LostFoundResponseImage extends Model
{
    use HasFactory;

    protected $connection = 'ntd_lost_found';
    protected $table = 'lost_found_response_images';

    protected $fillable = ['lost_found_id','lost_found_response_id','image_link','caption'];
}
