<?php

namespace App\Models\LostFound;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\LostFound\LostFoundResponseImage;
use App\Models\User;

class LostFoundResponse extends Model
{
    use HasFactory;

    protected $connection = 'ntd_lost_found';
    protected $table = 'lost_found_responses';

    protected $fillable = ['lost_found_id','user_id','description'];

    public function user()
    {
        return $this->setConnection('mysql')->belongsTo(User::class)->select('id','name','avatar');
    }

    public function lost_found_response_images()
    {
        return $this->hasMany(LostFoundResponseImage::class)->select('id','lost_found_id','lost_found_response_id','image_link','caption');
    }
}