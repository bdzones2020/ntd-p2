<?php

namespace App\Models\LostFound;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\LostFound\LostFoundResponse;
use App\Models\LostFound\LostFoundImage;
use App\Models\User;
use App\Models\Location\Division;
use App\Models\Location\District;
use App\Models\Location\Thana;

class LostFound extends Model
{
    use HasFactory;

    protected $connection = 'ntd_lost_found';
    protected $table = 'lost_founds';

    protected $fillable = ['user_id','lost_found_code','title','occurance_type','occurance_date','occurance_time','subject_type','subject_name','tags','division_id','district_id','thana_id','address','description','duration','status','is_archived'];

    public function user()
    {
        return $this->setConnection('mysql')->belongsTo(User::class)->select('id','name');
    }

    public function division()
    {
        return $this->belongsTo(Division::class)->select('id','name','slug');
    }

    public function district()
    {
        return $this->belongsTo(District::class)->select('id','division_id','name','slug');
    }

    public function thana()
    {
        return $this->belongsTo(Thana::class)->select('id','district_id','name','slug');
    }

    public function lost_found_images()
    {
        return $this->hasMany(LostFoundImage::class)->select('id','lost_found_id','image_link','caption');
    }

    public function first_image()
    {
        return $this->hasOne(LostFoundImage::class, 'lost_found_id', 'id')->select('id','lost_found_id','image_link');
    }

    public function lost_found_responses()
    {
        return $this->hasMany(LostFoundResponse::class)->select('id','lost_found_id','user_id','description','created_at')->with('lost_found_response_images');
    }
}
