<?php

use App\Http\Controllers\HumanResource\HumanResourceController;

Route::group(['prefix' => 'human-resource', 'as' => 'human-resource.'], function () {
    Route::get('/', [HumanResourceController::class, 'index'])->name('/');;
    Route::get('/faq', [HumanResourceController::class, 'faq'])->name('faq');
    Route::get('terms-&-conditions', [HumanResourceController::class, 'terms_conditions'])->name('terms-&-conditions');
    Route::get('about-us', [HumanResourceController::class, 'about_us'])->name('about-us');
});
