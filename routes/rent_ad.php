<?php

use App\Http\Controllers\RentAd\RentAdController;

Route::group(['prefix' => 'rent-ad', 'as' => 'rent-ad.'], function () {
    Route::get('/', [RentAdController::class, 'index'])->name('/');;
    Route::get('/faq', [RentAdController::class, 'faq'])->name('faq');
    Route::get('terms-&-conditions', [RentAdController::class, 'terms_conditions'])->name('terms-&-conditions');
    Route::get('about-us', [RentAdController::class, 'about_us'])->name('about-us');
});
