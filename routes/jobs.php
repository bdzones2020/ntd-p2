<?php

use App\Http\Controllers\Jobs\JobsController;

Route::group(['prefix' => 'jobs', 'as' => 'jobs.'], function () {
    Route::get('/', [JobsController::class, 'index'])->name('/');;
    Route::get('/faq', [JobsController::class, 'faq'])->name('faq');
    Route::get('terms-&-conditions', [JobsController::class, 'terms_conditions'])->name('terms-&-conditions');
    Route::get('about-us', [JobsController::class, 'about_us'])->name('about-us');
});
