<?php

use App\Http\Controllers\LostFound\LostFoundController;

Route::group(['prefix' => 'lost-found', 'as' => 'lost-found.'], function () {
    // public routes
    Route::get('/', [LostFoundController::class, 'index'])->name('/');
    Route::get('/details/{id}', [LostFoundController::class, 'details'])->name('details');
    Route::get('/faq', [LostFoundController::class, 'faq'])->name('faq');
    Route::get('terms-&-conditions', [LostFoundController::class, 'terms_conditions'])->name('terms-&-conditions');
    Route::get('about-us', [LostFoundController::class, 'about_us'])->name('about-us');

    // ajax call
    Route::get('/data-list', [LostFoundController::class, 'data_list'])->name('data-list');
    Route::post('/store-data', [LostFoundController::class, 'store_data'])->name('store-data');
    Route::get('/get-data/{id}', [LostFoundController::class, 'get_data'])->name('get-data');
    Route::patch('/update-data', [LostFoundController::class, 'update_data'])->name('update-data');
    Route::delete('/delete-data', [LostFoundController::class, 'delete_data'])->name('delete-data');

    // auth routes
    Route::group(['middleware' => ['auth']], function () {
        // Route::get('create', [LostFoundController::class, 'create'])->name('create');
        Route::post('store', [LostFoundController::class, 'store'])->name('store');
        Route::post('store-response', [LostFoundController::class, 'store_response'])->name('store-response');
    });
});