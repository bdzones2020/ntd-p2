<?php

use App\Http\Controllers\Exchange\ExchangeController;

Route::group(['prefix' => 'exchange', 'as' => 'exchange.'], function () {
    Route::get('/', [ExchangeController::class, 'index'])->name('/');;
    Route::get('/faq', [ExchangeController::class, 'faq'])->name('faq');
    Route::get('terms-&-conditions', [ExchangeController::class, 'terms_conditions'])->name('terms-&-conditions');
    Route::get('about-us', [ExchangeController::class, 'about_us'])->name('about-us');
});
