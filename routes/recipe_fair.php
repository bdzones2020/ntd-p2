<?php

use App\Http\Controllers\RecipeFair\RecipeFairController;

Route::group(['prefix' => 'recipe-fair', 'as' => 'recipe-fair.'], function () {
    Route::get('/', [RecipeFairController::class, 'index'])->name('/');;
    Route::get('/faq', [RecipeFairController::class, 'faq'])->name('faq');
    Route::get('terms-&-conditions', [RecipeFairController::class, 'terms_conditions'])->name('terms-&-conditions');
    Route::get('about-us', [RecipeFairController::class, 'about_us'])->name('about-us');
});
