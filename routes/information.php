<?php

use App\Http\Controllers\Information\InformationController;

Route::group(['prefix' => 'information', 'as' => 'information.'], function () {
    Route::get('/', [InformationController::class, 'index'])->name('/');;
    Route::get('/faq', [InformationController::class, 'faq'])->name('faq');
    Route::get('terms-&-conditions', [InformationController::class, 'terms_conditions'])->name('terms-&-conditions');
    Route::get('about-us', [InformationController::class, 'about_us'])->name('about-us');
});
