<?php

use App\Http\Controllers\TestController;

Route::group(['prefix' => 'test', 'as' => 'test.'], function () {
    // Route::get('/migrate-fresh', function(){
    //     $exitCode = Artisan::call('migrate:fresh --seed');
    //     $exitCode = Artisan::call('optimize:clear');
    //     return back();
    // });
    Route::get('/check-db', [TestController::class, 'check_db']);
});
