<?php

use App\Http\Controllers\HumanityService\HumanityServiceController;

Route::group(['prefix' => 'humanity-service', 'as' => 'humanity-service.'], function () {
    Route::get('/', [HumanityServiceController::class, 'index'])->name('/');;
    Route::get('/faq', [HumanityServiceController::class, 'faq'])->name('faq');
    Route::get('terms-&-conditions', [HumanityServiceController::class, 'terms_conditions'])->name('terms-&-conditions');
    Route::get('about-us', [HumanityServiceController::class, 'about_us'])->name('about-us');
});
