<?php

use App\Http\Controllers\SaleInfo\SaleInfoController;

Route::group(['prefix' => 'sale-info', 'as' => 'sale-info.'], function () {
    Route::get('/', [SaleInfoController::class, 'index'])->name('/');;
    Route::get('/faq', [SaleInfoController::class, 'faq'])->name('faq');
    Route::get('terms-&-conditions', [SaleInfoController::class, 'terms_conditions'])->name('terms-&-conditions');
    Route::get('about-us', [SaleInfoController::class, 'about_us'])->name('about-us');
});
