<?php

use App\Http\Controllers\Tsc\TscController;

Route::group(['prefix' => 'tsc', 'as' => 'tsc.'], function () {
    Route::get('/', [TscController::class, 'index'])->name('/');;
    Route::get('/faq', [TscController::class, 'faq'])->name('faq');
    Route::get('terms-&-conditions', [TscController::class, 'terms_conditions'])->name('terms-&-conditions');
    Route::get('about-us', [TscController::class, 'about_us'])->name('about-us');
});
