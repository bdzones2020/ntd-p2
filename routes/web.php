<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-parasites', function () {
    $exitCode = Artisan::call('optimize:clear');
    return back();
});

Route::get('/', [WebController::class, 'index'])->name('/');
Route::get('/home', [WebController::class, 'index'])->name('home');
Route::get('/faq', [WebController::class, 'faq'])->name('faq');
Route::get('terms-&-conditions', [WebController::class, 'terms_conditions'])->name('terms-&-conditions');
Route::get('about-us', [WebController::class, 'about_us'])->name('about-us');

Auth::routes();

// Route::group(['middleware' => ['auth']], function () {
//     Route::get('/home', [HomeController::class, 'home'])->name('home');
// });

require __DIR__.'/lost_found.php';
require __DIR__.'/exchange.php';
require __DIR__.'/sale_info.php';
require __DIR__.'/tsc.php';
require __DIR__.'/rent_ad.php';
require __DIR__.'/jobs.php';
require __DIR__.'/information.php';
require __DIR__.'/human_resource.php';
require __DIR__.'/blog.php';
require __DIR__.'/humanity_service.php';
require __DIR__.'/recipe_fair.php';
require __DIR__.'/test.php';

