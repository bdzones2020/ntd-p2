<?php

use App\Http\Controllers\Blog\BlogController;

Route::group(['prefix' => 'blog', 'as' => 'blog.'], function () {
    Route::get('/', [BlogController::class, 'index'])->name('/');;
    Route::get('/faq', [BlogController::class, 'faq'])->name('faq');
    Route::get('terms-&-conditions', [BlogController::class, 'terms_conditions'])->name('terms-&-conditions');
    Route::get('about-us', [BlogController::class, 'about_us'])->name('about-us');
});
