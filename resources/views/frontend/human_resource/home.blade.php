@extends('frontend.human_resource.index')
@section('title', 'মানবসম্পদ')
@section('meta_keywords', config('app.name') . 'মানবসম্পদ')
@section('human_resource_content')

<div class="container">
    <section class="padding-bottom-sm">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase">মানবসম্পদ</h4>
        </div>
    </section>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>
@endpush