@extends('layouts.app')

@section('topbar_links')
    <a href="{{ route('human-resource./') }}" class="btn-icon px-1 py-1" title="মানবসম্পদ" data-toggle="tooltip" data-placement="top">
        <i class="fa fa-home"></i> <span>মানবসম্পদ</span>
    </a>
    <a href="{{ route('human-resource.terms-&-conditions') }}" class="btn-icon px-1 py-1" title="শর্তাবলী" data-toggle="tooltip" data-placement="top">
        <i class="fa fa-list"></i> <span>শর্তাবলী</span>
    </a>
    <a href="{{ route('human-resource.faq') }}" class="btn-icon px-1 py-1" title="জিজ্ঞাসা" data-toggle="tooltip" data-placement="top">
        <i class="fa fa-question-circle-o"></i> <span>জিজ্ঞাসা</span>
    </a>
    <span class="dropdown">
        <a class="btn-icon px-1 py-1 dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-plus"></i> <span>প্রকাশ করুন</span>
        </a>
        <div class="dropdown-menu">
            <h5 class="dropdown-header text-light">Dropdown header</h5>
            <a class="dropdown-item" href="#">Link 1</a>
            <a class="dropdown-item" href="#">Link 2</a>
            <a class="dropdown-item" href="#">Link 3</a>
            <div class="dropdown-divider"></div>
            <h5 class="dropdown-header">Dropdown header</h5>
            <a class="dropdown-item" href="#">Another link</a>
        </div>
    </span>
@endsection

@section('content')
    @yield('human_resource_content')
@endsection