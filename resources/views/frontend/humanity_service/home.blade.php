@extends('frontend.humanity_service.index')
@section('title', 'মানবিক সেবা')
@section('meta_keywords', config('app.name') . 'মানবিক সেবা')
@section('humanity_service_content')

<div class="container">
    <section class="padding-bottom-sm">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase">মানবিক সেবা</h4>
        </div>
    </section>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>
@endpush