@extends('frontend.blog.index')
@section('title', 'মুক্তমঞ্চ')
@section('meta_keywords', config('app.name') . 'মুক্তমঞ্চ')
@section('blog_content')

<div class="container">
    <section class="padding-bottom-sm">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase">মুক্তমঞ্চ</h4>
        </div>
    </section>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>
@endpush