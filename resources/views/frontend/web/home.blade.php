@extends('frontend.web.index')
@section('meta_keywords', config('app.name') . 'Home')

@section('web_content')

<div class="container">
    <section class="pb-1">
        <div class="row row-sm justify-content-center">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <a href="{{ route('sale-info./') }}" class="card card-sm card-product-grid card-box">
                    <span class="box-title">
                        বিক্রয় তথ্য
                    </span>
                    <p>
                        দেশজুড়ে বিভিন্ন খুচরা ও পাইকারি দ্রব্যাদি, সবজি, ফল, মাছ, জমি, বাড়ি, গাড়ি ইত্যাদি ক্রয় বা বিক্রয়ের তথ্য।
                    </p>
                </a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <a href="{{ route('recipe-fair./') }}" class="card card-sm card-product-grid card-box">
                    <span class="box-title">
                        ব্যবসায়িক সেবা
                    </span>
                    <p>
                        ক্ষুদ্র ও মাঝারি পরিসরের বিভিন্ন ব্যবসার জন্যে ডিজিটাল ব্যবস্থাপনা ও সেবা ভিত্তিক অনলাইন সমাধান। 
                    </p>
                </a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <a href="{{ route('lost-found./') }}" class="card card-sm card-product-grid card-box">
                    <span class="box-title">
                        হারানো পাওয়া
                    </span>
                    <p>
                        কোন মানুষ বা পোষা প্রাণী বা বস্তু হারানো গেলে বা পাওয়া গেলে এখানে খুঁজুন বা প্রকাশ করুন।
                    </p>
                </a>
            </div>
            <div href="{{ route('human-resource./') }}" class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <a href="{{ route('human-resource./') }}" class="card card-sm card-product-grid card-box">
                    <span class="box-title">
                        মানবসম্পদ
                    </span>
                    <p>
                        বিভিন্ন পেশার দক্ষ প্রত্যেকটি মানুষ এই দেশের সম্পদ। নিজেকে নিবন্ধন করুন বা অন্যকে খুঁজুন।
                    </p>
                </a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <a href="{{ route('tsc./') }}" class="card card-sm card-product-grid card-box">
                    <span class="box-title">
                        ছাত্র শিক্ষক কেন্দ্র
                    </span>
                    <p>
                        বাংলাভাষার সকল শিক্ষক ও শিক্ষার্থির জন্যে জ্ঞান ও তথ্য আদান প্রদানের একটি সহজপ্রাপ্য কেন্দ্র।
                    </p>
                </a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <a href="{{ route('exchange./') }}" class="card card-sm card-product-grid card-box">
                    <span class="box-title">
                        অদল বদল
                    </span>
                    <p>
                        বৈধভাবে হস্তান্তর যোগ্য যে কোন জিনিষ পত্র অদল বদলের একটি সহজ উপায়।
                    </p>
                </a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <a href="{{ route('jobs./') }}" class="card card-sm card-product-grid card-box">
                    <span class="box-title">
                        কর্মসংস্থান
                    </span>
                    <p>
                        চুক্তি ভিত্তিক স্বাধীন কাজ বা চাকুরী যে কোন প্রকার কাজ খুঁজুন বা প্রকাশ করুন। 
                    </p>
                </a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <a href="{{ route('humanity-service./') }}" class="card card-sm card-product-grid card-box">
                    <span class="box-title">
                        মানবিক সেবা
                    </span>
                    <p>
                        অভাবী, দুস্থ, আর্ত পীড়িত, বিপদগ্রস্থ মানুষের জন্যে বা নিজের জন্যে সাহায্য খুঁজুন বা অন্যকে সাহায্য করুন।
                    </p>
                </a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <a href="{{ route('blog./') }}" class="card card-sm card-product-grid card-box">
                    <span class="box-title">
                        মুক্তমঞ্চ
                    </span>
                    <p>
                        গল্প, কবিতা, বা অন্যান্য সাহিত্য, নিজের মতামত, চিন্তা, দর্শন, অভিজ্ঞতা প্রকাশের একটি ব্লগ।
                    </p>
                </a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <a href="{{ route('rent-ad./') }}" class="card card-sm card-product-grid card-box">
                    <span class="box-title">
                        ভাড়ার বিজ্ঞাপন
                    </span>
                    <p>
                        বাসা ভাড়া, অফিস ভাড়া, গাড়ি বা যে বৈধ ভাড়ার যোগ্য তথ্য বিজ্ঞাপন দিন বা খুঁজুন।
                    </p>
                </a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <a href="{{ route('recipe-fair./') }}" class="card card-sm card-product-grid card-box">
                    <span class="box-title">
                        রান্না মেলা
                    </span>
                    <p>
                        রাঁধুনিদের রান্নার মাধ্যমে ঘরে বসে উপার্জনের এবং চাহিদা অনুযায়ী বিভিন্ন ভোজন রসিকদের স্বাদ মিটাতে একটা সহজ আয়োজন।
                    </p>
                </a>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <a href="{{ route('information./') }}" class="card card-sm card-product-grid card-box">
                    <span class="box-title">
                        প্রয়োজনীয় তথ্য
                    </span>
                    <p>
                        নাগরিক বা জাতীয় বা বিভিন্ন প্রয়োজনীয় তথ্য।
                    </p>
                </a>
            </div>
        </div>
    </section>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>
@endpush