@extends('frontend.web.index')
@section('title', 'শর্তাবলী')
@section('meta_keywords', config('app.name') . 'শর্তাবলী')

@section('web_content')

<div class="container">

    <section class="padding-bottom-sm">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase">শর্তাবলী</h4>
        </div>
    </section>

</div>
<!-- container end.// -->

@endsection

@push('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>

@endpush