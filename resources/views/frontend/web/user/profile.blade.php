@extends('frontend.web.user.index')

@section('title', Auth::user()->name . ' - Profile')

@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/flatpicker-4.5.7/css/flatpicker-4.5.7.min.css') }}">
    <script src="{{ asset('plugins/flatpicker-4.5.7/js/flatpicker-4.5.7.min.js') }}"></script>
    <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet" />
@endpush

@php $divisions = divisions() @endphp
@php $districts = districts() @endphp
@php $thanas = thanas() @endphp

@section('user_content')

<div class="container-fluid">

    <article class="card mb-3">
        <div class="card-body">

            <h4>
                {{ $profile->user->name }} - Profile
                <button id="editBtn" class="btn btn-sm btn-outline-info ml-2"><i class="fa fa-edit"></i> Edit</button>
                <button id="cancelBtn" class="btn btn-sm btn-outline-danger hide ml-2" onclick="location.reload();"><i class="fa fa-close"></i> Cancel</button>
            </h4>

            <hr>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form id="profileUpdateForm" method="post" action="{{ route('profile.update') }}">
                @csrf

                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="full_name" class=""><strong>Full Name</strong></label>
                        <input type="text" class="form-control" name="full_name" value="{{ !empty($profile->full_name) ? $profile->full_name : old('full_name') }}" placeholder="Full Name" />
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="dob" class=""><strong>Date of Birth</strong></label>
                        <input type="text" class="form-control" id="dob" name="dob" value="{{ !empty($profile->dob) ? date('d-m-Y',strtotime($profile->dob)) : old('dob') }}" placeholder="{{ date('d-m-Y', strtotime('-12 years')) }}" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label><strong>Gender</strong></label>
                        <div class="pt-1">
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" name="gender" value="MALE" {{ !empty($profile->gender) && $profile->gender == 'MALE' ? 'checked' : ''  }}/>
                                <span class="custom-control-label"> MALE </span>
                            </label>
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" name="gender" value="FEMALE" {{ !empty($profile->gender) && $profile->gender == 'FEMALE' ? 'checked' : ''  }}/>
                                <span class="custom-control-label"> FEMALE </span>
                            </label>
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" name="gender" value="COMMON" {{ !empty($profile->gender) && $profile->gender == 'COMMON' ? 'checked' : ''  }}/>
                                <span class="custom-control-label"> COMMON </span>
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6 col-sm-12 form-group">
                        <label><strong>Religion</strong></label>
                        <div class="pt-1">
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" checked="" type="radio" name="religion" value="ISLAM" {{ !empty($profile->religion) && $profile->religion == 'ISLAM' ? 'checked' : ''  }}/>
                                <span class="custom-control-label"> ISLAM </span>
                            </label>
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" name="religion" value="HINDUISM" {{ !empty($profile->religion) && $profile->religion == 'HINDUISM' ? 'checked' : ''  }}/>
                                <span class="custom-control-label"> HINDUISM </span>
                            </label>
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" name="religion" value="CHRISTIANISM" {{ !empty($profile->religion) && $profile->religion == 'CHRISTIANISM' ? 'checked' : ''  }}/>
                                <span class="custom-control-label"> CHRISTIANISM </span>
                            </label>
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" name="religion" value="BUDDHISM" {{ !empty($profile->religion) && $profile->religion == 'BUDDHISM' ? 'checked' : ''  }}/>
                                <span class="custom-control-label"> BUDDHISM </span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="personal_contact_number" class=""><strong>Personal Contact Number</strong></label>
                        <input type="tel" id="personal_contact_number" name="personal_contact_number" class="form-control quantity_class" value="{{ !empty($profile->personal_contact_number) ? $profile->personal_contact_number : old('personal_contact_number') }}" minlength="11" maxlength="11" placeholder="01XXXXXXXXX" />
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="personal_contact_email" class=""><strong>Personal Contact Email</strong></label>
                        <input type="email" class="form-control" id="personal_contact_email" name="personal_contact_email" value="{{ !empty($profile->personal_contact_email) ? $profile->personal_contact_email : old('personal_contact_email') }}" placeholder="Ex. john@gmail.com"/>
                    </div>
                </div>

                <hr class="mt-0">

                <div class="row">
                    <div class="col-12">
                        <strong>Present Address</strong>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="present_division_id" class="control-label">Division</label>
                        <select id="present_division_id" name="present_division_id" class="form-control select2" required="">
                            @if(count($divisions) > 0)
                                @foreach($divisions as $division)
                                <option value="{{ $division->id }}">{{ $division->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="present_district_id" class="">District</label>
                        <select id="present_district_id" name="present_district_id" class="form-control select2" required="">
                            @if(count($districts) > 0)
                                @foreach($districts as $district)
                                <option value="{{ $district->id }}">{{ $district->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="present_thana_id" class="">Thana</label>
                        <select id="present_thana_id" name="present_thana_id" class="form-control select2" required="">
                            @if(count($thanas) > 0)
                                @foreach($thanas as $thana)
                                <option value="{{ $thana->id }}">{{ $thana->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="present_postal_code" class="">Postal Code</label>
                        <input type="number" class="form-control" id="present_postal_code" name="present_postal_code" value="{{ !empty($profile->present_postal_code) ? $profile->present_postal_code : old('present_postal_code') }}" placeholder="1205" required=""></input>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 form-group">
                        <label for="present_address" class="">Present Address Line</label>
                        <input type="text" class="form-control" id="present_address" name="present_address" value="{{ !empty($profile->present_address) ? $profile->present_address : old('present_address') }}" placeholder="Address Line" required="">
                    </div>
                </div>

                <hr class="mt-0">

                <div class="row">
                    <div class="col-12">
                        <strong>Permanent Address</strong>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="permanent_division_id" class="control-label">Division</label>
                        <select id="permanent_division_id" name="permanent_division_id" class="form-control select2" required="">
                            @if(count($divisions) > 0)
                                @foreach($divisions as $division)
                                <option value="{{ $division->id }}">{{ $division->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="permanent_district_id" class="">District</label>
                        <select id="permanent_district_id" name="permanent_district_id" class="form-control select2" required="">
                            @if(count($districts) > 0)
                                @foreach($districts as $district)
                                <option value="{{ $district->id }}">{{ $district->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="permanent_thana_id" class="">Thana</label>
                        <select id="permanent_thana_id" name="permanent_thana_id" class="form-control select2" required="">
                            @if(count($thanas) > 0)
                                @foreach($thanas as $thana)
                                <option value="{{ $thana->id }}">{{ $thana->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="permanent_postal_code" class="">Postal Code</label>
                        <input type="number" class="form-control" id="permanent_postal_code" name="permanent_postal_code" value="{{ !empty($profile->permanent_postal_code) ? $profile->permanent_postal_code : old('permanent_postal_code') }}" placeholder="1205" required=""></input>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 form-group">
                        <label for="permanent_address" class="">Permanent Address Line</label>
                        <input type="text" class="form-control" id="permanent_address" name="permanent_address" value="{{ !empty($profile->permanent_address) ? $profile->permanent_address : old('permanent_address') }}" placeholder="Address Line" required="">
                    </div>
                </div>

                <hr class="mt-0">

                <div class="row">
                    <div class="col-12">
                        <strong>Emergency Contact Person</strong>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="emergency_contact_person" class=""><strong>Full Name</strong></label>
                        <input type="text" class="form-control" id="emergency_contact_person" name="emergency_contact_person" value="{{ !empty($profile->emergency_contact_person) ? $profile->emergency_contact_person : old('emergency_contact_person') }}" placeholder="Contact Person Full Name"/>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="emergency_contact_relation" class=""><strong>Relation</strong></label>
                        <input type="text" class="form-control" id="emergency_contact_relation" name="emergency_contact_relation" value="{{ !empty($profile->emergency_contact_relation) ? $profile->emergency_contact_relation : old('emergency_contact_relation') }}" placeholder="Relation"/>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                        <label for="emergency_contact_number" class=""><strong>Contact Number</strong></label>
                        <input type="tel" id="emergency_contact_number" name="emergency_contact_number" class="form-control quantity_class" value="{{ !empty($profile->emergency_contact_number) ? $profile->emergency_contact_number : old('emergency_contact_number') }}" minlength="11" maxlength="11" placeholder="01XXXXXXXXX" />
                    </div>
                </div>

                <hr class="mt-0">

                <div class="row">
                    <div class="col-12 form-group">
                        <button id="submitBtn" type="submit" class="btn btn-primary hide pull-right" disabled="">Update</button>
                    </div>
                </div>

            </form>

        </div>
    </article>

</div>

@endsection

@push('scripts')
<script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
<script>
    $(document).ready(function(){

        var profile = {!! json_encode($profile) !!};
        var divisions = {!! json_encode($divisions) !!};
        var districts = {!! json_encode($districts) !!};
        var thanas = {!! json_encode($thanas) !!};
        var maxDate = {!! json_encode(date('d-m-Y', strtotime('-12 years'))) !!};

        $('#profileUpdateForm :input').prop('disabled',true);

        $('#editBtn').click(function(){
            $('#profileUpdateForm :input').prop('disabled',false);
            $('#submitBtn').show();
            $(this).hide();
            $('#cancelBtn').show();
        });
        
        flatpickr("#dob", {
            dateFormat: "d-m-Y",
            maxDate: maxDate
        });

        $('.select2').select2({
            placeholder: "Select One",
            allowClear: true
        });

        $('.select2').val('').trigger('change');

        if(profile['present_division_id'])
        {
            $('#present_division_id').val(profile['present_division_id']).trigger('change');
        }

        if(profile['present_district_id'])
        {
            $('#present_district_id').val(profile['present_district_id']).trigger('change');
        }

        if(profile['present_thana_id'])
        {
            $('#present_thana_id').val(profile['present_thana_id']).trigger('change');
        }

        if(profile['permanent_division_id'])
        {
            $('#permanent_division_id').val(profile['permanent_division_id']).trigger('change');
        }

        if(profile['permanent_district_id'])
        {
            $('#permanent_district_id').val(profile['permanent_district_id']).trigger('change');
        }

        if(profile['permanent_thana_id'])
        {
            $('#permanent_thana_id').val(profile['permanent_thana_id']).trigger('change');
        }

        $('#present_division_id').change(function(){
            var divisionId = parseInt($(this).val());
            var district_reset = '';
            if($(this).val() > 0){
                $.each(districts, function(d, district){
                    if(parseInt(district['division_id']) == divisionId){
                        district_reset = district_reset + '<option value="'+district['id']+'">'+district['name']+'</option>';
                    }
                });
                $('#present_district_id').html('');
                $('#present_district_id').html(district_reset);
                $('#present_district_id').val('').trigger('change');
            }
        });

        $('#present_district_id').change(function(){
            var districtId = parseInt($(this).val());
            var thana_reset = '';
            if($(this).val() != ''){
                $.each(thanas, function(t, thana){
                    if(parseInt(thana['district_id']) == districtId){
                        thana_reset = thana_reset + '<option value="'+thana['id']+'">'+thana['name']+'</option>';
                    }
                });
                $('#present_thana_id').html('');
                $('#present_thana_id').html(thana_reset);
                $('#present_thana_id').val('').trigger('change');
            }
        });

        $('#permanent_division_id').change(function(){
            var divisionId = parseInt($(this).val());
            var district_reset = '';
            if($(this).val() != ''){
                $.each(districts, function(d, district){
                    if(parseInt(district['division_id']) == divisionId){
                        district_reset = district_reset + '<option value="'+district['id']+'">'+district['name']+'</option>';
                    }
                });
                $('#permanent_district_id').html('');
                $('#permanent_district_id').html(district_reset);
                $('#permanent_district_id').val('').trigger('change');
            }
        });

        $('#permanent_district_id').change(function(){
            var districtId = parseInt($(this).val());
            var thana_reset = '';
            if($(this).val() != ''){
                $.each(thanas, function(t, thana){
                    if(parseInt(thana['district_id']) == districtId){
                        thana_reset = thana_reset + '<option value="'+thana['id']+'">'+thana['name']+'</option>';
                    }
                });
                $('#permanent_thana_id').html('');
                $('#permanent_thana_id').html(thana_reset);
                $('#permanent_thana_id').val('').trigger('change');
            }
        });

    });
</script>

@endpush