@extends('frontend.web.user.index')

@section('title', Auth::user()->name .  ' - Carts')

@section('user_content')

<div class="container-fluid">

    <article class="card mb-3">
        <div class="card-body">

            <h4>
                {{ $profile->user->name }} - Carts
            </h4>

            <hr>

            <div class="row">
                @if(count($carts) > 0)
                    @foreach($carts as $c => $cart)

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="text-center">
                                        <strong class="text-dark">Cart</strong> at <strong class="text-primary">{{ $cart->shop->shop_name }}</strong>
                                    </div>
                                    <hr>
                                    <div class="col-12 mb-2 fw-600">
                                        <span>Total Items</span>
                                        <span class="pull-right">{{ $cart->cart_items->count() }}</span>
                                    </div>
                                    <div class="col-12 mb-2 fw-600">
                                        <span>Total Quantity</span>
                                        <span class="pull-right">{{ $cart->cart_items->sum('quantity') }}</span>
                                    </div>
                                    <div class="col-12 mb-2 fw-600">
                                        <span>Total Price</span>
                                        <span class="pull-right">{{ $cart->total_price }}</span>
                                    </div>
                                    <div class="col-12 mb-2 text-right">
                                        <a href="" class="btn btn-primary fw-600">See Details</a>
                                        <a href="" class="btn btn-primary fw-600">Check Out</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                @endif
            </div>

        </div>
    </article>

</div>

@endsection

@push('scripts')

<script>
    $(document).ready(function(){
        //
    });
</script>

@endpush