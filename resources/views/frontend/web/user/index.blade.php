@extends('layouts.app')

@push('sidebar')

    @include('frontend.web.user.inc.sidebar')

@endpush

@section('content')

    @yield('user_content')

@endsection

@push('scripts')

    <script type="text/javascript">
        function readURL(input)
        {
            if (input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function (e)
                {
                    $('#photo_preview')
                    .attr('src', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function(){
            $('#updatePhotoBtn').click(function(){
                $('#photo').trigger('click');
                $('#submitPhotoBtnBox').show();
            });

            $('#submitPhotoBtn').click(function(){
                $('#photoUpdateForm').submit();
            });
        });
    </script>

@endpush
