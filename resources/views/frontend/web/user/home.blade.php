@extends('frontend.web.user.index')

@section('title', Auth::user()->name . ' - Profile')

@push('styles')

@endpush

@section('user_content')

<div class="container-fluid">

    <article class="card mb-3">
        <div class="card-body">

            <h4>
                {{ $profile->user->name }} - Profile
            </h4>

            <hr>

            <div class="row">
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="row">
                        <div class="col-sm-6 mb-3 text-center">
                            <img class="rounded-circle img-fluid img-md border" src="{{ asset('web/images/avatars/avatar3.jpg') }}">
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-2">
                                <span class="text-secondary">User Name</span> : <strong class="text-dark">{{ $profile->user->name }}</strong>
                            </div>
                            <div class="mb-2">
                                <span class="text-secondary">Registered Contact Number</span> : <strong class="text-dark">{{ $profile->user->contact_number }}</strong>
                            </div>
                            <div class="mb-2">
                                <span class="text-secondary">Registered Email</span> : <strong class="text-dark">{{ $profile->user->email }}</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            Full Name
                        </div>
                        <div class="col-md-8">
                            {{ $profile->full_name }}
                        </div>
                    </div>
                </div>
            </div>

            



            <article class="card-group card-stat">
                <figure class="card bg">
                    <div class="p-3">
                        <h4 class="title">38</h4>
                        <span>Orders</span>
                    </div>
                </figure>
                <figure class="card bg">
                    <div class="p-3">
                        <h4 class="title">5</h4>
                        <span>Wishlists</span>
                    </div>
                </figure>
                <figure class="card bg">
                    <div class="p-3">
                        <h4 class="title">12</h4>
                        <span>Awaiting delivery</span>
                    </div>
                </figure>
                <figure class="card bg">
                    <div class="p-3">
                        <h4 class="title">50</h4>
                        <span>Delivered items</span>
                    </div>
                </figure>
            </article>


        </div>
    </article>

</div>

@endsection

@push('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>

@endpush