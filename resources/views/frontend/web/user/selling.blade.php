@extends('frontend.web.user.profile.index')
@section('title', 'Profile - ' . Auth::user()->name)

@push('styles')

@endpush

@section('profile_content')
    <article class="card">
        <div class="card-body">

            <div class="row">
                <div class="col-md-4">
                    <figure class="card card-product-grid">
                        <div class="img-wrap">
                            <img src="{{ asset('web/images/items/1.jpg') }}">
                        </div>
                        <figcaption class="info-wrap">
                            <a href="#" class="title mb-2">Hot sale unisex New Design Shirt for sport polo shirts latest
                                design</a>
                            <div class="price-wrap mb-3">
                                <span class="price">$32.00-$40.00</span>
                                <small class="text-muted">/per item</small>
                            </div>
                            <a href="#" class="btn btn-outline-primary"> <i class="fa fa-pen"></i> Edit </a>
                            <a href="#" class="btn btn-primary"> <i class="fa fa-eye"></i> View </a>

                            <hr>
                            <a href="#" class="btn btn-success btn-block"> Promote </a>
                        </figcaption>
                    </figure>
                </div>

                <div class="col-md-4">
                    <figure class="card card-product-grid">
                        <div class="img-wrap">
                            <img src="{{ asset('web/images/items/2.jpg') }}">
                        </div>
                        <figcaption class="info-wrap">
                            <a href="#" class="title mb-2">High Quality Winter PU Rain Jacket with Padding for Men's
                                outdoor</a>
                            <div class="price-wrap mb-3">
                                <span class="price">$41.00-$50.00</span>
                                <small class="text-muted">/per item</small>
                            </div>
                            <a href="#" class="btn btn-outline-primary"> <i class="fa fa-pen"></i> Edit </a>
                            <a href="#" class="btn btn-primary"> <i class="fa fa-eye"></i> View </a>

                            <hr>
                            <a href="#" class="btn btn-success btn-block"> Promote </a>
                        </figcaption>
                    </figure>
                </div>

                <div class="col-md-4">
                    <figure class="card card-product-grid">
                        <div class="img-wrap">
                            <img src="{{ asset('web/images/items/1.jpg') }}">
                        </div>
                        <figcaption class="info-wrap">
                            <a href="#" class="title mb-2">Cheap and Best demo clothe with latest Fashion styles for
                                Men</a>
                            <div class="price-wrap mb-3">
                                <span class="price">$32.00-$40.00</span>
                                <small class="text-muted">/per item</small>
                            </div>
                            <a href="#" class="btn btn-outline-primary"> <i class="fa fa-pen"></i> Edit </a>
                            <a href="#" class="btn btn-primary"> <i class="fa fa-eye"></i> View </a>

                            <hr>
                            <a href="#" class="btn btn-success btn-block"> Promote </a>
                        </figcaption>
                    </figure>
                </div>

                <div class="col-md-4">
                    <figure class="card card-product-grid">
                        <div class="img-wrap">
                            <img src="{{ asset('web/images/items/4.jpg') }}">
                        </div>
                        <figcaption class="info-wrap">
                            <a href="#" class="title mb-2">Cheap and Best demo clothe with latest Fashion styles for
                                Men</a>
                            <div class="price-wrap mb-3">
                                <span class="price">$32.00-$40.00</span>
                                <small class="text-muted">/per item</small>
                            </div>
                            <a href="#" class="btn btn-outline-primary"> <i class="fa fa-pen"></i> Edit </a>
                            <a href="#" class="btn btn-primary"> <i class="fa fa-eye"></i> View </a>

                            <hr>
                            <a href="#" class="btn btn-success btn-block"> Promote </a>
                        </figcaption>
                    </figure>
                </div>

                <div class="col-md-4">
                    <figure class="card card-product-grid">
                        <div class="img-wrap">
                            <img src="{{ asset('web/images/items/5.jpg') }}">
                        </div>
                        <figcaption class="info-wrap">
                            <a href="#" class="title mb-2">Cheap and Best demo clothe with latest Fashion styles for
                                Men</a>
                            <div class="price-wrap mb-3">
                                <span class="price">$32.00-$40.00</span>
                                <small class="text-muted">/per item</small>
                            </div>
                            <a href="#" class="btn btn-outline-primary"> <i class="fa fa-pen"></i> Edit </a>
                            <a href="#" class="btn btn-primary"> <i class="fa fa-eye"></i> View </a>

                            <hr>
                            <a href="#" class="btn btn-success btn-block"> Promote </a>
                        </figcaption>
                    </figure>
                </div>
            </div>

        </div>
    </article>
@endsection

@push('scripts')

    <script>
    </script>

@endpush
