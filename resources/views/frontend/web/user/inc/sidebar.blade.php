@php $route = Route::current() @endphp
@php $profile = profile() @endphp
<div id="sidebar-wrapper" class="theme-bg">
    <ul class="sidebar-nav theme-bg">
        <li class="list-group-item text-center text-light">
            <div class="mb-2">
                <button id="updatePhotoBtn" class="btn btn-sm btn-outline-light" style="position: absolute;top: 0;right: 0;"><i class="fa fa-edit"></i> change</button>
                <img id="photo_preview" class="rounded-circle img-fluid img-md border" src="{{ !empty($profile->photo) ? asset('images/profiles/'.$profile->photo) : asset('web/images/avatars/avatar3.jpg') }}">
                <div id="submitPhotoBtnBox" class="mt-2 hide">
                    <button id="submitPhotoBtn" class="btn btn-sm btn-info">Update</button>
                </div>
            </div>
            <div class="mb-2">
                <i class="fa fa-user-circle-o text-warning"></i> <strong>{{ $profile->user->name }}</strong>
            </div>
            <div class="mb-2">
                <i class="fa fa-mobile text-warning"></i> <strong>{{ $profile->user->contact_number }}</strong>
            </div>
            <div class="mb-2">
                <i class="fa fa-envelope text-warning"></i> <strong>{{ $profile->user->email ? $profile->user->email : 'No email found' }}</strong>
            </div>
            <form id="photoUpdateForm" action="{{route('profile.update.photo')}}" method="post" action="" enctype="multipart/form-data" style="display: none;">
                @csrf
                <input type="file" class="hidden-file-input" id="photo" name="photo" onchange="readURL(this);" accept=".jpg, .jpeg, .png, .gif">
            </form>
        </li>
        <li class="list-group-item">
            <a href="{{ route('profile./') }}" class="">
                Profile
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ route('profile.account') }}" class="">
                Account
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ route('profile.carts') }}" class="">
                Carts
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ route('profile.my-orders') }}" class="">
                Orders
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ route('profile.my-orders') }}" class="">
                Products
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ route('profile.my-orders') }}" class="">
                Wishlist
            </a>
        </li>
        <!-- <li class="list-group-item">
            <a href="{{ route('profile.businesses.index') }}" class="">
                My Businesses
            </a>
        </li> -->

    </ul>
</div>
