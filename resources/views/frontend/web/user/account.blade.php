@extends('frontend.web.user.index')

@section('title', Auth::user()->name . ' - Account Setting')

@section('user_content')

<div class="container-fluid">

    <article class="card mb-3">
        <div class="card-body">

            <h4>
                {{ $profile->user->name }} - Account Setting
                <button id="editBtn" class="btn btn-sm btn-outline-info ml-2"><i class="fa fa-edit"></i> Edit</button>
                <button id="cancelBtn" class="btn btn-sm btn-outline-danger hide ml-2" onclick="location.reload();"><i class="fa fa-close"></i> Cancel</button>
            </h4>

            <hr>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form id="accountUpdateForm" method="post" action="{{ route('profile.account.update') }}">
                @csrf

                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-12 form-group text-secondary">
                                <strong>Registration Credentials</strong>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="name" class=""><strong>User Name</strong></label>
                                <input type="text" class="form-control" name="name" value="{{ !empty($profile->user->name) ? $profile->user->name : old('name') }}" placeholder="User Name" required />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="contact_number" class=""><strong>Registered Contact Number</strong></label>
                                <input type="tel" id="contact_number" name="contact_number" class="form-control quantity_class" value="{{ !empty($profile->user->contact_number) ? $profile->user->contact_number : old('contact_number') }}" minlength="11" maxlength="11" placeholder="01XXXXXXXXX" required />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="email" class=""><strong>Registered Contact Email</strong></label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ !empty($profile->user->email) ? $profile->user->email : old('email') }}" placeholder="Ex. john@gmail.com"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="row hide" id="passwordSwitch">
                            <div class="col-12 form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="changePassword">
                                    <label class="custom-control-label text-secondary" for="changePassword"><strong>Change Password</strong></label>
                                </div>
                            </div>
                        </div>

                        <div id="passwordBox" class="hide">
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <label for="current_password" class=""><strong>Current Password</strong></label>
                                    <input type="password" class="form-control" name="current_password" value="" required />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <label for="password" class=""><strong>New Password</strong></label>
                                    <input type="password" name="password" class="form-control" id="password" placeholder="Password" required="">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <label for="password_confirmation" class=""><strong>Confirm New Password</strong></label>
                                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="mt-0">

                <div class="row">
                    <div class="col-12 form-group">
                        <button id="submitBtn" type="submit" class="btn btn-primary hide" disabled="">Update</button>
                    </div>
                </div>

            </form>

        </div>
    </article>

</div>

@endsection

@push('scripts')

<script>
    $(document).ready(function(){

        var profile = {!! json_encode($profile) !!};

        $('#accountUpdateForm :input').prop('disabled',true);
        $('#passwordBox :input').prop('required',false);

        $('#editBtn').click(function(){
            $('#accountUpdateForm :input').prop('disabled',false);
            $('#submitBtn').show();
            $(this).hide();
            $('#cancelBtn').show();
            $('#passwordSwitch').show();
        });

        $('#changePassword').change(function(){
            if($(this).prop('checked'))
            {
                $('#passwordBox').show();
                $('#passwordBox :input').prop('required',true);
            }else{
                $('#passwordBox').hide();
                $('#passwordBox :input').prop('required',false);
            }
        });

    });
</script>

@endpush