@extends('frontend.web.user.index')
@section('title', 'Business List')

@push('styles')

@endpush

@section('user_content')

<div class="container-fluid">

    <article class="card mb-3">
        <div class="card-body">

            <h4>
                Business List
                <a class="btn btn-sm btn-outline-info pull-right" href="{{ route('profile.businesses.create') }}">Create Business</a>
            </h4>

            <table class="table">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Business Name</th>
                        <th>Type</th>
                        <th>Dashboard</th>
                        <th>Website</th>
                        <th>Action</th>
                    </tr>
                </thead>

                @if(count($businesses) > 0)
                    <tbody>
                        @foreach($businesses as $b => $business)
                            <tr>
                                <td>{{ $b+1 }}</td>
                                <td>{{ $business->business_name }}</td>
                                <td>{{ $business->business_type }}</td>
                                <td>
                                    <a class="btn btn-sm btn-outline-info" href="{{ route('dashboard./',['businessSlug' => $business->slug]) }}">Dashboard</a>
                                </td>
                                <td>{{ $business->business_email }}</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle" data-toggle="dropdown"></button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="">Edit</a>
                                            <a class="dropdown-item" href="">View</a>
                                            <a class="dropdown-item" href="">Delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                @endif
            </table>

        </div>
    </article>

</div>

@endsection

@push('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>

@endpush