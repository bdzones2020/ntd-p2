@extends('frontend.web.user.profile.index')
@section('title', $business->business_name. ' Details')

@push('styles')

@endpush

@section('profile_content')

<div class="row">
    <div class="container">
        <h4>{{ $business->business_name }} <small id="editBtn" class="pull-right theme-link cursor-pointer" title="Edit Identical Document" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i> Edit</small></h4>

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 ">
                        <label  class="pt-2">Business Name: <strong>{{$business->business_name ?? $business->business_name}}</strong></label>
                    </div>
                    <div class="col-md-4">
                        <label class="pt-2">Business Type: <strong>{{$business->business_type ?? $business->business_type}}</strong></label>
                    </div>
                    <div class="col-md-4">
                        <label  class="pt-2">Owner Type: <strong>{{$business->owner_type ?? $business->owner_type}}</strong></label>
                    </div>

                    <div class="col-md-4">
                        <label class="pt-2">Shop Name: <strong>{{$business->shop->shop_name ?? $business->shop->shop_name}}</strong></label>
                    </div>
                    <div class="col-md-4 ">
                        <label  class="pt-2">Shop Email: <strong>{{$business->shop->shop_email ?? $business->shop->shop_email}}</strong></label>
                    </div>
                    <div class="col-md-4">
                        <label  class="pt-2">Shop Contact Number: <strong>{{$business->shop->shop_contact_numbers ?? $business->shop->shop_contact_numbers}}</strong></label>
                    </div>

                    <div class="col-md-4">
                        <label class="pt-2">Division:
                            <strong>
                                @foreach(divisions() as $division)
                                    @if(($business->shop->division_id && $business->shop->division_id == $division->id)) {{$division->name}}@endif
                                @endforeach
                            </strong>
                        </label>
                    </div>
                    <div class="col-md-4 ">
                        <label  class="pt-2">District:
                            <strong>
                                @foreach(districts() as $district)
                                    @if(($business->shop->district_id && $business->shop->district_id == $district->id)) {{$district->name}}@endif
                                @endforeach
                            </strong>
                        </label>
                    </div>
                    <div class="col-md-4">
                        <label  class="pt-2">Thana:
                            <strong>
                                @foreach(thanas() as $thana)
                                    @if(($business->shop->thana_id && $business->shop->thana_id == $thana->id)) {{$thana->name}}@endif
                                @endforeach
                            </strong>
                        </label>
                    </div>


                    <div class="col-md-4">
                        <label class="pt-2">Address: <strong>{{$business->shop->address ?? $business->shop->address}}</strong></label>
                    </div>
                    <div class="col-md-4 ">
                        <label  class="pt-2">Postal Code: <strong>{{$business->shop->postal_code ?? $business->shop->postal_code}}</strong></label>
                    </div>
                    <div class="col-md-4">
                        <label  class="pt-2">Status: <strong>{{$business->status ?? $business->status}}</strong></label>
                    </div>

                </div>
                <hr>
                <form method="post" action="{{ route('profile.businesses.identifier.store',$business->id) }}" enctype="multipart/form-data">
                    @csrf
                    @if($business->identical_document)
                        <input id="id" name="id" type="hidden" value="{{$business->identical_document->id}}"/>
                    @endif
                    <input id="business_id" name="business_id" type="hidden" value="{{$business->id}}"/>
                    <div class="form-row form-group">
                        <div class="col-md-2 text-md-right">
                            <label for="trade_license_no" class="pt-2">Trade License Number</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="trade_license_no" name="trade_license_no" placeholder="Trade License number"
                             value="{{!empty($business->identical_document) ? $business->identical_document->trade_license_no : ''}}">
                        </div>
                        <div class="col-md-2 text-md-right">
                            <label for="tin" class="pt-2">Tin Number</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="tin" name="tin" placeholder="Tin number"
                             value="{{!empty($business->identical_document) ? $business->identical_document->tin : ''}}">
                        </div>

                    </div>
                    <div class="form-row form-group">
                        <div class="col-md-2 text-md-right">
                            <label for="trade_license_image" class="pt-2">Trade License Image (Max:5MB)</label>
                        </div>
                        <div class="col-md-4">
                            <input type="file" class="form-control" id="trade_license_image" name="trade_license_image" onchange="loadFile(event,'tradeImg')"><br>
                            @if(!empty($business->identical_document) && !empty($business->identical_document->getFirstMediaUrl(App\Constants\Media::BUSINESS_TRADE)))
                                <img id="tradeImg" style="width: 120px; height: auto;" src="{{ $business->identical_document->getFirstMediaUrl(App\Constants\Media::BUSINESS_TRADE) }}" alt="logo" class="mt-2 img-fluid img-thumbnail">
                            @else
                                <img src="https://via.placeholder.com/120x120?text=Placeholder+Image" id="tradeImg" style="width: 120px; height: auto;">
                            @endif

                            @if ($errors->has('trade_license_image'))
                                <span class="text-danger">
                                    <b>*{{ $errors->first('trade_license_image') }}</b>
                                </span>
                            @endif
                        </div>

                        <div class="col-md-2 text-md-right">
                            <label for="tin_image" class="pt-2">Tin Image (Max:5MB)</label>
                        </div>
                        <div class="col-md-4">
                            <input type="file" class="form-control" id="tin_image" name="tin_image" onchange="loadFile(event,'tinImg')"><br>
                            @if(!empty($business->identical_document) && !empty($business->identical_document->getFirstMediaUrl(App\Constants\Media::BUSINESS_TIN)))
                                <img id="tinImg" style="width: 120px; height: auto;" src="{{ $business->identical_document->getFirstMediaUrl(App\Constants\Media::BUSINESS_TIN) }}" alt="logo" class="mt-2 img-fluid img-thumbnail">
                            @else
                                <img src="https://via.placeholder.com/120x120?text=Placeholder+Image" id="tinImg" style="width: 120px; height: auto;">
                            @endif

                            @if ($errors->has('tin_image'))
                                <span class="text-danger">
                                    <b>*{{ $errors->first('tin_image') }}</b>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-md-2 text-md-right">
                            <label for="bin" class="pt-2">Bin Number</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="bin" name="bin" placeholder="Bin number"
                             value="{{!empty($business->identical_document) ? $business->identical_document->bin : ''}}">
                        </div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-md-2 text-md-right">
                            <label for="bin_image" class="pt-2">Bin Image (Max:5MB)</label>
                        </div>
                        <div class="col-md-4">
                            <input type="file" class="form-control" id="bin_image" name="bin_image"  onchange="loadFile(event,'binImg')"><br>
                            @if(!empty($business->identical_document) && !empty($business->identical_document->getFirstMediaUrl(App\Constants\Media::BUSINESS_BIN)))
                                <img id="binImg" style="width: 120px; height: auto;" src="{{ $business->identical_document->getFirstMediaUrl(App\Constants\Media::BUSINESS_BIN) }}" alt="logo" class="mt-2 img-fluid img-thumbnail">
                            @else
                                <img src="https://via.placeholder.com/120x120?text=Placeholder+Image" id="binImg" style="width: 120px; height: auto;">
                            @endif

                            @if ($errors->has('bin_image'))
                                <span class="text-danger">
                                    <b>*{{ $errors->first('bin_image') }}</b>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-md-12 text-right">
                            <button type="submit"  id="updateBtn" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script>
    $(document).ready(function(){
        loadFile = function(event, id) {
            var output = document.getElementById(id);
            output.src = URL.createObjectURL(event.target.files[0]);
        };

        $('.form-control').prop('disabled',true);

        $('#editBtn').click(function(){
            if($('.form-control').prop('disabled'))
            {
                $(this).html('');
                $(this).html('<i class="fa fa-close"></i> Cancel');
                $('.form-control').prop('disabled',false);
                $('#updateBtn').prop('disabled',false);
            }else{
                $(this).html('');
                $(this).html('<i class="fa fa-edit"></i> Edit');
                $('.form-control').prop('disabled',true);
                $('#updateBtn').prop('disabled',true);
            }
        });

    });
</script>

@endpush
