@extends('frontend.web.user.index')

@section('title', 'Create Business')

@push('styles')
    <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet" />
@endpush

@php $divisions = divisions() @endphp
@php $districts = districts() @endphp
@php $thanas = thanas() @endphp

@section('user_content')

<div class="container-fluid">

    <article class="card mb-3">
        <div class="card-body">

            <h4>Create Business</h4>

            <hr>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="post" action="{{ route('profile.businesses.store') }}">
                @csrf

                <div class="row">
                    <div class="col-md-4 col-sm-6 form-group">
                        <label for="business_name" class=""><strong>Business Name</strong></label>
                        <input type="text" class="form-control" id="business_name" name="business_name" value="{{ old('business_name') }}" placeholder="Business Name" required="" autofocus="">
                    </div>
                    <!-- <div class="col-md-4 col-sm-6 form-group">
                        <label><strong>Business Type</strong></label>
                        <div class="pt-1">
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input businessType" type="radio" name="business_type" value="shop" checked="">
                                <span class="custom-control-label"> SHOP </span>
                            </label>
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input businessType" type="radio" name="business_type" value="service" disabled="true">
                                <span class="custom-control-label"> SERVICE </span>
                            </label>
                        </div>
                    </div> -->
                </div>

                <input type="hidden" name="business_type" value="shop">

                <hr>

                <div class="row">
                    <div class="col-12">
                        <h5>Contacts</h5>
                    </div>
                    <div class="col-md-4 col-sm-6 form-group">
                        <label for="shop_email" class="">Business Email <smal class="text-blue">(optional)</smal></label>
                        <input type="email" class="form-control" id="shop_email" name="shop_email" placeholder="business@email.com">
                    </div>
                    <div class="col-md-4 col-sm-6 form-group">
                        <label for="shop_contact_numbers" class="">Business Contact Number</label>
                        <div class="contactNumberInputs">
                            <div class="input-group">
                                <input type="tel" class="form-control quantity_class" id="shop_contact_numbers" name="shop_contact_numbers[]" placeholder="01XXXXXXXXX" required="">
                                <!-- <div class="input-group-append">
                                    <span class="input-group-button"><button type="button" id="addBtn" class="btn btn-info">Add More</button></span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-12">
                        <h5>Location</h5>
                    </div>
                    <div class="col-md-4 col-sm-6 form-group">
                        <label for="division_id" class="control-label">Division</label>
                        <select id="division_id" name="division_id" class="form-control select2" required="">
                            @if(count($divisions) > 0)
                                @foreach($divisions as $division)
                                <option value="{{ $division->id }}">{{ $division->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-6 form-group">
                        <label for="district_id" class="">District</label>
                        <select id="district_id" name="district_id" class="form-control select2" required="">
                            @if(count($districts) > 0)
                                @foreach($districts as $district)
                                <option value="{{ $district->id }}">{{ $district->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-6 form-group">
                        <label for="thana_id" class="">Thana</label>
                        <select id="thana_id" name="thana_id" class="form-control select2" required="">
                            @if(count($thanas) > 0)
                                @foreach($thanas as $thana)
                                <option value="{{ $thana->id }}">{{ $thana->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-6 form-group">
                        <label for="postal_code" class="">Postal Code</label>
                        <input type="number" class="form-control" id="postal_code" name="postal_code" class="form-control" placeholder="1205"></input>
                    </div>
                    <div class="col-md-4 col-sm-6 form-group">
                        <label for="address" class="">Business Address</label>
                        <input type="text" class="form-control" id="address" name="address" placeholder="Business Address" required="">
                    </div>
                </div>

                <!-- <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-6 form-group">
                        <label for="map_location" class="">Map Location <span class="text-blue">(optional but help greatly to find out your business faster)</span></label>
                        <div class="input-group">
                            <textarea class="form-control" id="map_location" rows="3" name="map_location" placeholder="paste your embed map here from google map"></textarea>
                            <div class="input-group-append">
                                <span class="input-group-text bg-white">
                                    <a target="_blank" href="https://www.google.com/maps/" class="btn btn-md btn-info">Map</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div> -->

                <hr>

                <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-6 form-group">
                        <label class="custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" checked="" required="">
                            <div class="custom-control-label"> I am agree with <a href="#">terms and contitions</a> </div>
                        </label>
                    </div>
                    <div class="col-md-4 col-sm-6 form-group">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>

            </form>

        </div>
    </article>

</div>

@endsection

@push('scripts')
<script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
<script>
    $(document).ready(function(){

        var contactNumberInput = '<div class="input-group"><input type="tel" class="form-control quantity_class" id="shop_contact_numbers" name="shop_contact_numbers[]" placeholder="01XXXXXXXXX" required=""><div class="input-group-append"><span class="input-group-button"><button type="button" onclick="$(this).parent().parent().parent().remove()" class="btn btn-danger"> X </button></span></div></div>';

        $('#addBtn').click(function(){
            $('.contactNumberInputs').append(contactNumberInput);
        });

        var divisions = {!! json_encode($divisions) !!};
        var districts = {!! json_encode($districts) !!};
        var thanas = {!! json_encode($thanas) !!};

        $('.select2').select2({
            placeholder: "Select One",
            allowClear: true
        });

        $('.select2').val('').trigger('change');

        $('#division_id').change(function(){
            var divisionId = parseInt($(this).val());
            var district_reset = '';
            if($(this).val() != ''){
                $.each(districts, function(d, district){
                    if(parseInt(district['division_id']) == divisionId){
                        district_reset = district_reset + '<option value="'+district['id']+'">'+district['name']+'</option>';
                    }
                });
                $('#district_id').html('');
                $('#district_id').html(district_reset);
                $('#district_id').val('').trigger('change');
            }
        });

        $('#district_id').change(function(){
            var districtId = parseInt($(this).val());
            var thana_reset = '';
            if($(this).val() != ''){
                $.each(thanas, function(t, thana){
                    if(parseInt(thana['district_id']) == districtId){
                        thana_reset = thana_reset + '<option value="'+thana['id']+'">'+thana['name']+'</option>';
                    }
                });
                $('#thana_id').html('');
                $('#thana_id').html(thana_reset);
                $('#thana_id').val('').trigger('change');
            }
        });

    });
</script>

@endpush