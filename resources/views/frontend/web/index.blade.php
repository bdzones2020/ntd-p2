@extends('layouts.app')

@section('topbar_links')
    <a href="{{ route('home') }}" class="btn-icon px-1 py-1" title="বনিপাতা" data-toggle="tooltip" data-placement="top">
        <i class="fa fa-home"></i> <span>বনিপাতা</span>
    </a>
    <a href="{{ route('terms-&-conditions') }}" class="btn-icon px-1 py-1" title="শর্তাবলী" data-toggle="tooltip" data-placement="top">
        <i class="fa fa-list"></i> <span>শর্তাবলী</span>
    </a>
    <a href="{{ route('faq') }}" class="btn-icon px-1 py-1" title="জিজ্ঞাসা" data-toggle="tooltip" data-placement="top">
        <i class="fa fa-question-circle-o"></i> <span>জিজ্ঞাসা</span>
    </a>
@endsection

@section('content')
    @yield('web_content')
@endsection