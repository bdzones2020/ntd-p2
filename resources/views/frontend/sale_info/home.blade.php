@extends('frontend.sale_info.index')
@section('title', 'বিক্রয় তথ্য')
@section('meta_keywords', config('app.name') . 'বিক্রয় তথ্য')
@section('sale_info_content')

<div class="container">
    <section class="padding-bottom-sm">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase">বিক্রয় তথ্য</h4>
        </div>
    </section>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>
@endpush