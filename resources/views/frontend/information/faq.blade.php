@extends('frontend.information.index')
@section('title', 'জিজ্ঞাসা')
@section('meta_keywords', config('app.name') . 'জিজ্ঞাসা')

@section('information_content')

<div class="container">

    <section class="padding-bottom-sm">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase">জিজ্ঞাসা</h4>
        </div>
    </section>

</div>
<!-- container end.// -->

@endsection

@push('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>

@endpush