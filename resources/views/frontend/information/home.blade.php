@extends('frontend.information.index')
@section('title', 'প্রয়োজনীয় তথ্য')
@section('meta_keywords', config('app.name') . 'প্রয়োজনীয় তথ্য')
@section('information_content')

<div class="container">
    <section class="padding-bottom-sm">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase">প্রয়োজনীয় তথ্য</h4>
        </div>
    </section>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>
@endpush