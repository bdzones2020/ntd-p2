@php $divisions = divisions() @endphp
@php $districts = districts() @endphp
@php $thanas = thanas() @endphp

@push('styles')
    <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet" />
@endpush

<div class="row">
    <div class="col-12">
        <label class="control-label"><strong>স্থান</strong></label>
    </div>
    <div class="col-lg-4 col-md-6 form-group">
        <label for="division_id" class="control-label">বিভাগ</label>
        <select id="division_id" name="division_id" class="form-control division_id_input select2" required="">
            @if(count($divisions) > 0)
                @foreach($divisions as $division)
                <option value="{{ $division->id }}">{{ $division->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-lg-4 col-md-6 form-group">
        <label for="district_id" class="">জেলা</label>
        <select id="district_id" name="district_id" class="form-control district_id_input select2" required="">
            @if(count($districts) > 0)
                @foreach($districts as $district)
                <option value="{{ $district->id }}">{{ $district->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-lg-4 col-md-6 form-group">
        <label for="thana_id" class="">থানা</label>
        <select id="thana_id" name="thana_id" class="form-control thana_id_input select2" required="">
            @if(count($thanas) > 0)
                @foreach($thanas as $thana)
                <option value="{{ $thana->id }}">{{ $thana->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>

@push('scripts')
    <script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var divisions = {!! json_encode($divisions) !!};
            var districts = {!! json_encode($districts) !!};
            var thanas = {!! json_encode($thanas) !!};

            $('.select2').select2({
                placeholder: "নির্বাচন করুন",
                allowClear: true
            });

            $('.select2').val('').trigger('change');

            $('.division_id_input').change(function(){
                var divisionId = parseInt($(this).val());
                var district_reset = '';
                if($(this).val() != ''){
                    $.each(districts, function(d, district){
                        if(parseInt(district['division_id']) == divisionId){
                            district_reset = district_reset + '<option value="'+district['id']+'">'+district['name']+'</option>';
                        }
                    });
                    $('.district_id_input').html('');
                    $('.district_id_input').html(district_reset);
                    $('.district_id_input').val('').trigger('change');
                }
            });

            $('.district_id_input').change(function(){
                var districtId = parseInt($(this).val());
                var thana_reset = '';
                if($(this).val() != ''){
                    $.each(thanas, function(t, thana){
                        if(parseInt(thana['district_id']) == districtId){
                            thana_reset = thana_reset + '<option value="'+thana['id']+'">'+thana['name']+'</option>';
                        }
                    });
                    $('.thana_id_input').html('');
                    $('.thana_id_input').html(thana_reset);
                    $('.thana_id_input').val('').trigger('change');
                }
            });
        });
    </script>
@endpush