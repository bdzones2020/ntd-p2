<!-- row -->
<div class="row">
    <div class="col-sm-4">
        @include('backend.helpers.name')
    </div>
    <div class="col-sm-4">
        @include('backend.helpers.serial_no')
    </div>
</div>
<!-- ./row -->
<!-- row -->
<div class="row">
    <div class="col-sm-4">
        @include('backend.helpers.image')
    </div>
    
    <div class="col-sm-4">
        @include('backend.helpers.image_shape')
    </div>

    @if(!empty($editRow->image))
    <div class="col-sm-4">
        <div class="form-group">
            <label for="old_image" class="col-sm-4 control-label">Old image</label>
            <div class="col-sm-8">
                <img src="{{ asset('img_category/'.$editRow->image) }}" alt="" class="img-responsive col-sm-12 mt-3">
            </div>
        </div>
    </div>
    @endif
</div>
<!-- ./row -->
<!-- row -->
<div class="row">
    <div class="col-sm-4">
        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
            <label for="website" class="col-sm-12 control-label fw500">Website<i class="text-danger">*</i></label>
            <div class="col-sm-12">
                <input id="website" type="text" class="form-control" name="website" value="{{ !empty($editRow->website) ? $editRow->website : old('website') }}" placeholder="http://your-website.com">
                @if ($errors->has('website'))
                    <span class="help-block">
                        <strong>{{ $errors->first('website') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>  
    <div class="col-sm-4">
        @include('backend.helpers.status')
    </div>
</div>
<!-- ./row -->
<!-- row -->
<div class="row">
    <div class="col-sm-8">
        @include('backend.helpers.details')
    </div>
</div>
<!-- ./row -->