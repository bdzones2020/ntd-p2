@php $divisions = divisions() @endphp
@php $districts = districts() @endphp
@php $thanas = thanas() @endphp

<div class="row">
    <div class="col-12 mb-3">
        <a class="btn btn-primary btn-block"><i class="fa fa-map-marker"></i> your device location</a>
    </div>
    <div class="col-md-12">
        <div id="locationSlider" class="carousel slide" data-ride="carousel" data-interval="false">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <ul class="list-group">
                        <li class="list-group-item bg-light text-blue text-center fw-600">
                            Divisions
                        </li>
                        @if(count($divisions) > 0)
                            @foreach($divisions as $division)
                                <li class="list-group-item p-0 text-center">
                                    <a class="d-inline-block pt-1 selectedLocation" href="" title="Set Location to {{ $division->name }}" data-name="{{ $division->name }}">{{ $division->name }}</a>
                                    <a type="button" class="btn btn-light pull-right districtListBtn" data-id="{{ $division->id }}" data-name="{{ $division->name }}" href="#locationSlider" role="button" data-slide-to="1" title="Inside {{ $division->name }}"><i class="fa fa-angle-double-right"></i></a>
                                </li>
                            @endforeach
                        @endif            
                    </ul>
                </div>
                <div class="carousel-item">
                    <ul class="list-group">
                        <li class="list-group-item bg-light text-blue text-center fw-600">
                            Division
                        </li>
                        <li class="list-group-item p-0 text-center">
                            <a type="button" class="btn btn-light pull-left backToLocations" href="#locationSlider" role="button" data-slide-to="0" title="Back"><i class="fa fa-angle-double-left"></i></a>
                            <strong class="d-inline-block pt-1 selectedDivision"></strong>
                            <a type="button" class="btn btn-info pull-right" role="button" title="selected" disabled=""><i class="fa fa-check-square"></i></a>
                        </li>
                        <li class="list-group-item bg-light text-blue text-center fw-600">
                            Districts
                        </li>
                        @if(count($districts) > 0)
                            @foreach($districts as $district)
                                <li class="list-group-item p-0 text-center districtList division_{{ $district->division_id }}">
                                    <a class="d-inline-block pt-1 selectedLocation" href="" title="Set Location to {{ $district->name }}" data-name="{{ $district->name }}">{{ $district->name }}</a>
                                    <a type="button" class="btn btn-light pull-right thanaListBtn" data-id="{{ $district->id }}" data-name="{{ $district->name }}" href="#locationSlider" role="button" data-slide-to="2" title="Inside {{ $district->name }}"><i class="fa fa-angle-double-right"></i></a>
                                </li>
                            @endforeach
                        @endif            
                    </ul>
                </div>
                <div class="carousel-item">
                    <ul class="list-group">
                        <li class="list-group-item bg-light text-blue text-center fw-600">
                            Division
                        </li>
                        <li class="list-group-item p-0 text-center">
                            <a type="button" class="btn btn-light pull-left backToLocations" href="#locationSlider" role="button" data-slide-to="0" title="Back"><i class="fa fa-angle-double-left"></i></a>
                            <strong class="d-inline-block pt-1 selectedDivision"></strong>
                            <a type="button" class="btn btn-info pull-right" role="button" title="selected" disabled=""><i class="fa fa-check-square"></i></a>
                        </li>
                        <li class="list-group-item bg-light text-blue text-center fw-600">
                            District
                        </li>
                        <li class="list-group-item p-0 text-center">
                            <a type="button" class="btn btn-light pull-left backToDivision" href="#locationSlider" role="button" data-slide-to="1" title="" data-name=""><i class="fa fa-angle-double-left"></i></a>
                            <strong class="d-inline-block pt-1 selectedDistrict"></strong>
                            <a type="button" class="btn btn-info pull-right" role="button" title="selected" disabled=""><i class="fa fa-check-square"></i></a>
                        </li>
                        <li class="list-group-item bg-light text-blue text-center fw-600">
                            Thana
                        </li>
                        @if(count($thanas) > 0)
                            @foreach($thanas as $thana)
                                <li class="list-group-item p-0 text-center thanaList district_{{ $thana->district_id }}">
                                    <a type="button" class="d-inline-block pt-1 selectedLocation" href="" data-id="{{ $thana->id }}" data-name="{{ $thana->name }}" data-details="thana" title="Set Location to {{ $thana->name }}" style="padding-bottom: 9.5px;">{{ $thana->name }}</a>
                                    <a id="selectedThana_{{ $thana->id }}" type="button" class="btn btn-info pull-right selectedThana hide" role="button" title="selected" disabled=""><i class="fa fa-check-square"></i></a>
                                </li>
                            @endforeach
                        @endif            
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    $(document).ready(function(){

        $('#locationSlider').carousel("pause");
        $('.districtList').hide();
        $('.thanaList').hide();

        $('.districtListBtn').click(function(){
            $('.districtList').hide();
            $('.selectedDivision').html('');
            $('.selectedDivision').html($(this).data('name'));
            $('.backToDivision').data('name',$(this).data('name'));
            $('.division_'+$(this).data('id')).show();
            setLocation($(this).data('name'));
        });

        $('.thanaListBtn').click(function(){
            $('.thanaList').hide();
            $('.selectedDistrict').html('');
            $('.selectedDistrict').html($(this).data('name'));
            $('.district_'+$(this).data('id')).show();
            setLocation($(this).data('name'));
        });

        $('.selectedLocation').click(function(e){
            e.preventDefault();
            if($(this).data('details') == 'thana')
            {
                $('.selectedThana').hide();
                $('#selectedThana_'+$(this).data('id')).show();
            }
            setLocation($(this).data('name'));
            $('#locationModal').modal('hide');
        });

        $('.backToDivision').click(function(){
            setLocation($(this).data('name')); 
        });

        $('.backToLocations').click(function(){
           setLocation('Locations');
        });

        function setLocation(locationName)
        {
            $('#locationNav').html('');
            $('#locationNav').html(locationName);
        }

    });
</script>

@endpush