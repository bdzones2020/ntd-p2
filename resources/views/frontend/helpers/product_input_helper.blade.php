@push('styles')
    <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/tagInputs/tagsinput.css') }}" rel="stylesheet" />
@endpush

<input id="businessSlug" type="hidden" name="businessSlug" value="{{ isset($businessSlug) ? $businessSlug : '' }}">

<!-- row -->
<div class="row bg-light">
    <div class="col-md-12">
        <div class="col-sm-12">
            <h5>Basic Information <small class="text-danger">(required)</small></h5>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-12 control-label fw500">Name</label>
            <div class="col-md-12">
                <input class="form-control" id="name" name="name" value="{{ !empty($businessSlug) ? (!empty($editRow->product->name) ? $editRow->product->name : old('name')) : (!empty($editRow->name) ? $editRow->name : old('name')) }}" placeholder="Product Name" required></input>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
            <label for="amount" class="col-md-12 control-label fw500">Amount</label>
            <div class="col-md-12">
                <div class="input-group">
                    <input type="text" class="form-control quantity__class" id="amount" name="amount" value="{{ !empty($businessSlug) ? (!empty($editRow->product->amount) ? $editRow->product->amount : old('amount')) : (!empty($editRow->amount) ? $editRow->amount : old('amount')) }}" placeholder="0" required></input>
                    <div class="input-group-append">
                        <select name="unit" class="form-control" id="unit" required>
                            <option value="pc">pc (piece)</option>
                            <option value="pc">pr (pair)</option>
                            <option value="pc">set (set)</option>
                            <option value="gm">gm (gram)</option>
                            <option value="kg">kg (kilogram)</option>
                            <option value="ml">ml (mililitre)</option>
                            <option value="l">l (litre)</option>
                        </select>
                    </div>
                </div>
                @if ($errors->has('amount'))
                    <span class="help-block">
                        <strong>{{ $errors->first('amount') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    @if(isset($businessSlug))
        <div class="col-md-3">
            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                <label for="price" class="col-md-12 control-label fw500">Price at your shop</label>
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="number" class="form-control" id="price" name="price" value="{{ !empty($editRow->price) ? $editRow->price : old('price') }}" step="0.1" min="0.0" placeholder="0.00" required></input>
                        <div class="input-group-append">
                            <span class="input-group-text bg-white"><strong>{{ baseCurrency() }}</strong></span>
                        </div>
                    </div>
                    @if ($errors->has('price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('price') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
        @if($business && $business->business_settings->wholesale_only)
            <div class="col-md-3">
                <div class="form-group{{ $errors->has('min_order_qty') ? ' has-error' : '' }}">
                    <label for="min_order_qty" class="col-md-12 control-label fw500">Minimum Order Quantity</label>
                    <div class="col-md-12">
                        <div class="input-group">
                            <input type="number" class="form-control" id="min_order_qty" name="min_order_qty" value="{{ !empty($editRow->min_order_qty) ? $editRow->min_order_qty : old('min_order_qty') }}" min="1" placeholder="0.00" required></input>
                        </div>
                        @if ($errors->has('min_order_qty'))
                            <span class="help-block">
                                <strong>{{ $errors->first('min_order_qty') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        @endif
    @endif

</div>
<!-- ./row -->
<hr>

<!-- row -->
<div class="row">
    <div class="col-md-12">
        <div class="col-sm-12">
            <h5>Categorisation <small class="text-info">(optional)</small></h5>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <label for="group_id" class="">Group</label>
                <select id="group_id" name="group_id" class="form-control select2">
                    <option value="0">Select Group</option>
                    @if(count($groups) > 0)
                        @foreach($groups as $group)
                        <option value="{{ $group->id }}" {{ !empty($editRow->product->group_id) && $editRow->product->group_id == $group->id ? 'selected' : '' }}>{{ $group->name }}</option>
                        @endforeach
                    @endif
                </select>
                @if ($errors->has('group_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('group_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <label for="category_id" class="">Category</label>
                <select id="category_id" name="category_id" class="form-control select2">
                    <option value="0">Select Category</option>
                    @if(count($categories) > 0)
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}" {{ !empty($editRow->product->category_id) && $editRow->product->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                        @endforeach
                    @endif
                </select>
                @if ($errors->has('category_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('category_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group{{ $errors->has('subcategory_id') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <label for="subcategory_id" class="">Subcategory</label>
                <select id="subcategory_id" name="subcategory_id" class="form-control select2">
                    <option value="0">Select Subcategory</option>
                    @if(count($subcategories) > 0)
                        @foreach($subcategories as $subcategory)
                        <option value="{{ $subcategory->id }}" {{ !empty($editRow->product->subcategory_id) && $editRow->product->subcategory_id == $subcategory->id ? 'selected' : '' }}>{{ $subcategory->name }}</option>
                        @endforeach
                    @endif
                </select>
                @if ($errors->has('subcategory_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subcategory_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <div class="col-sm-12">
            <h5>Brand & Type <small class="text-info">(optional)</small></h5>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('brand_id') ? ' has-error' : '' }}">
            <div class="col-md-12 mb-2">
                <label for="brand_id">Brand</label>
                <select id="brand_id" name="brand_id" class="form-control select2">
                    <option value="0">Select Brand</option>
                    @if(count($brands) > 0)
                        @foreach($brands as $brand)
                        <option value="{{ $brand->id }}" {{ !empty($editRow->product->brand_id) && $editRow->product->brand_id == $brand->id ? 'selected' : '' }}>{{ $brand->name }}</option>
                        @endforeach
                    @endif
                </select>
                @if ($errors->has('brand_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('brand_id') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-12 form-group">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="addBrand">
                    <label class="custom-control-label text-secondary" for="addBrand"><strong> Add Brand</strong> (if required)</label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 hide" id="addBrandBox">
        <div class="form-group{{ $errors->has('brand_name') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <label for="brand_name">Add Brand Name</label>
                <input class="form-control" id="brand_name" name="brand_name" value="" placeholder="Brand Name" />
                @if ($errors->has('brand_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('brand_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('product_type_id') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <label for="product_type_id" class="">Product Type</label>
                <select id="product_type_id" name="product_type_id" class="form-control select2">
                    <option value="0">Select Product Type</option>
                    @if(count($product_types) > 0)
                        @foreach($product_types as $product_type)
                        <option value="{{ $product_type->id }}" {{ !empty($editRow->product->product_type_id) && $editRow->product->product_type_id == $product_type->id ? 'selected' : '' }}>{{ $product_type->name }}</option>
                        @endforeach
                    @endif
                </select>
                @if ($errors->has('product_type_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('product_type_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <div class="col-sm-12">
            <h5>VAT & Tags <small class="text-info">(optional)</small></h5>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('vat') ? ' has-error' : '' }}">
            <label for="vat" class="col-md-12 control-label fw500">VAT</label>
            <div class="col-md-12">
                <div class="input-group">
                    <input type="number" class="form-control" id="vat" name="vat" value="{{ !empty($businessSlug) ? (!empty($editRow->product->vat) ? $editRow->product->vat : old('vat')) : (!empty($editRow->vat) ? $editRow->vat : old('vat')) }}" step="0.1" min="0.0" placeholder="0.00"></input>
                    <div class="input-group-append">
                        <span class="input-group-text bg-white"><strong>%</strong></span>
                    </div>
                </div>
                @if ($errors->has('vat'))
                    <span class="help-block">
                        <strong>{{ $errors->first('vat') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
            <label for="tags" class="col-md-12 control-label fw500">Tags</label>
            <div class="col-md-12">
                <input class="form-control" type="text" id="tags" name="tags" data-role="tagsinput" value="{{ !empty($businessSlug) ? (!empty($editRow->product->tags) ? $editRow->product->tags : old('tags')) : (!empty($editRow->tags) ? $editRow->tags : old('tags')) }}">
            </div>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <div class="col-sm-12">
            <h5>Description <small class="text-info">(optional)</small></h5>
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            <div class="col-sm-12">
                <textarea name="description" id="description" class="form-control" cols="1" rows="2">{{ !empty($businessSlug) ? (!empty($editRow->product_details->description) ? $editRow->product_details->description : old('description')) : (!empty($editRow->product_details->description) ? $editRow->product_details->description : old('description')) }}</textarea>
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <div class="col-sm-12">
            <h5>Specifications <small class="text-info">(optional)</small></h5>
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group{{ $errors->has('specifications') ? ' has-error' : '' }}">
            <div class="col-sm-12">
                <textarea name="specifications" id="specifications" class="form-control" cols="1" rows="2">{{ !empty($businessSlug) ? (!empty($editRow->product_details->specifications) ? $editRow->product_details->specifications : old('specifications')) : (!empty($editRow->product_details->specifications) ? $editRow->product_details->specifications : old('specifications')) }}</textarea>
                @if ($errors->has('specifications'))
                    <span class="help-block">
                        <strong>{{ $errors->first('specifications') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <div class="col-sm-12">
            <h5>Meta Information <small class="text-info">(optional)</small></h5>
        </div>
    </div>
    <div class="col-md-9 mb-2">
        <div class="form-group{{ $errors->has('meta_keywords') ? ' has-error' : '' }}">
            <label for="meta_keywords" class="col-sm-12 fw500">Meta Keywords <small class="text-info">(keywords should be lowercase)</small></label>
            <div class="col-sm-12">
                <input type="text" id="meta_keywords" name="meta_keywords" class="form-control" value="{{ !empty($businessSlug) ? (!empty($editRow->product_details->meta_keywords) ? $editRow->product_details->meta_keywords : old('meta_keywords')) : (!empty($editRow->product_details->meta_keywords) ? $editRow->product_details->meta_keywords : old('meta_keywords')) }}">
                @if ($errors->has('meta_keywords'))
                    <span class="help-block">
                        <strong>{{ $errors->first('meta_keywords') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
            <label for="meta_description" class="col-sm-12 fw500">Meta Description</label>
            <div class="col-sm-12">
                <textarea name="meta_description" id="meta_description" class="form-control" cols="1" rows="2">{{ !empty($businessSlug) ? (!empty($editRow->product_details->meta_description) ? $editRow->product_details->meta_description : old('meta_description')) : (!empty($editRow->product_details->meta_description) ? $editRow->product_details->meta_description : old('meta_description')) }}</textarea>
                @if ($errors->has('meta_description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('meta_description') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
    <script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('plugins/tagInputs/tagsinput.js') }}"></script>
    <script type="text/javascript">

        $(document).ready(function(){

            var editRow = {!! json_encode($editRow) !!};
            var businessSlug = $('#businessSlug').val();
            var groups = {!! json_encode($groups) !!};
            var categories = {!! json_encode($categories) !!};
            var subcategories = {!! json_encode($subcategories) !!};
            var groupId = {!! json_encode($groupId) !!};
            var categoryId = {!! json_encode($categoryId) !!};
            var subcategoryId = {!! json_encode($subcategoryId) !!};

            $('.select2').select2({
                placeholder: "Select One",
                allowClear: true
            });

            $('.select2').val('').trigger('change');

            if(businessSlug){
                if(editRow && editRow['product']['group_id'] != 0){
                    $('#group_id').val(editRow['product']['group_id']).trigger('change');
                }
            }else{
                if(editRow && editRow['group_id'] != 0){
                    $('#group_id').val(editRow['group_id']).trigger('change');
                }
            }

            if(businessSlug){
                if(editRow && editRow['product']['category_id'] != 0){
                    $('#category_id').val(editRow['product']['category_id']).trigger('change');
                }
            }else{
                if(editRow && editRow['category_id'] != 0){
                    $('#category_id').val(editRow['category_id']).trigger('change');
                }
            }

            if(businessSlug){
                if(editRow && editRow['product']['subcategory_id'] != 0){
                    $('#subcategory_id').val(editRow['product']['subcategory_id']).trigger('change');
                }
            }else{
                if(editRow && editRow['subcategory_id'] != 0){
                    $('#subcategory_id').val(editRow['subcategory_id']).trigger('change');
                }
            }

            if(groupId && groupId != 0){
                $('#group_id').val(groupId).trigger('change');
            }

            if(categoryId && categoryId != 0){
                $('#category_id').val(categoryId).trigger('change');
            }

            if(subcategoryId && subcategoryId != 0){
                $('#subcategory_id').val(subcategoryId).trigger('change');
            }

            if(businessSlug){
                if(editRow && editRow['product']['brand_id'] != 0){
                    $('#brand_id').val(editRow['product']['brand_id']).trigger('change');
                }
            }else{
                if(editRow && editRow['brand_id'] != 0){
                    $('#brand_id').val(editRow['brand_id']).trigger('change');
                }
            }

            if(businessSlug){
                if(editRow && editRow['product']['product_type_id'] != 0){
                    $('#product_type_id').val(editRow['product']['product_type_id']).trigger('change');
                }
            }else{
                if(editRow && editRow['product_type_id'] != 0){
                    $('#product_type_id').val(editRow['product_type_id']).trigger('change');
                }
            }

            $('#group_id').change(function(){
                var checlGroupId = parseInt($(this).val());
                var category_reset = '';
                if($(this).val() != ''){
                    $.each(categories, function(d, category){
                        if(parseInt(category['group_id']) == checlGroupId){
                            category_reset = category_reset + '<option value="'+category['id']+'">'+category['name']+'</option>';
                        }
                    });
                    $('#category_id').html('');
                    $('#category_id').html(category_reset);
                    $('#category_id').val('').trigger('change');
                }
            });

            $('#category_id').change(function(){
                var checkCategoryId = parseInt($(this).val());
                var subcategory_reset = '';
                if($(this).val() != ''){
                    $.each(subcategories, function(d, subcategory){
                        if(parseInt(subcategory['category_id']) == checkCategoryId){
                            subcategory_reset = subcategory_reset + '<option value="'+subcategory['id']+'">'+subcategory['name']+'</option>';
                        }
                    });
                    $('#subcategory_id').html('');
                    $('#subcategory_id').html(subcategory_reset);
                    $('#subcategory_id').val('').trigger('change');
                }
            });

            $('#addBrand').change(function(){
                if($(this).prop('checked'))
                {
                    $('#addBrandBox').show();
                }else{
                    $('#addBrandBox').hide();
                }
            });
        });
    </script>

@endpush