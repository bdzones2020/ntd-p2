<div class="form-group{{ $errors->has('shape') ? ' has-error' : '' }}">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12 mb-2">
                <strong class="control-label fw500">Select a Shape <small>(if required)</small></strong>
            </div>
            <div class="col-sm-12 fw500">
                <label class="custom-control custom-radio">
                    <input class="custom-control-input shape" id="none" class="shape" name="shape" type="radio" value="none">
                    <span class="custom-control-label"> None </span>
                </label>
                <label class="custom-control custom-radio">
                    <input class="custom-control-input shape" id="square" class="shape" name="shape" type="radio" value="square">
                    <span class="custom-control-label"> Square (will resize to 900px * 900px) </span>
                </label>
                <label class="custom-control custom-radio">
                    <input class="custom-control-input shape" id="potrait" class="shape" name="shape" type="radio" value="potrait">
                    <span class="custom-control-label"> Potrait (will resize to 600px * 900px) </span>
                </label>
                <label class="custom-control custom-radio">
                    <input class="custom-control-input shape" id="landscape" class="shape" name="shape" type="radio" value="landscape">
                    <span class="custom-control-label"> Landscape (will resize to 900px * 600px) </span>
                </label>
                <label class="custom-control custom-radio">
                    <input class="custom-control-input shape" id="custom" class="shape" name="shape" type="radio" value="custom">
                    <span class="custom-control-label"> Custom (Set Width * Height) </span>
                </label>
            </div>
            <div class="col-sm-12 mt-3" id="customShape">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('width') ? ' has-error' : '' }}">
                            <label for="width" class="col-sm-12 control-label fw500">Width (px)</label>
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <input id="width" type="text" class="form-control quantity_class" name="width" value="{{ !empty($editRow->width) ? $editRow->width : old('width') }}" placeholder="0" disabled="true">
                                </div>
                                @if ($errors->has('width'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('width') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('height') ? ' has-error' : '' }}">
                            <label for="height" class="col-sm-12 control-label fw500">height (px)</label>
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <input id="height" type="text" class="form-control quantity_class" name="height" value="{{ !empty($editRow->height) ? $editRow->height : old('height') }}" placeholder="0" disabled="true">
                                </div>
                                @if ($errors->has('height'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('height') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->has('shape'))
            <div class="col-sm-12">
                <span class="help-block">
                    <strong>{{ $errors->first('shape') }}</strong>
                </span>
            </div>
            @endif
        </div>
    </div>
</div>

@push('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        $('#customShape').hide();

        $('.shape').change(function(){
            if($(this).val() == 'custom'){
                $('#customShape').show();
                $('#width').prop('disabled',false);
                $('#height').prop('disabled',false);
            }else{
                $('#customShape').hide();
                $('#width').prop('disabled',true);
                $('#height').prop('disabled',true);
            }
        });
    });
</script>

@endpush