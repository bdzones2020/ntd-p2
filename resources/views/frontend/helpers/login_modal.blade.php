<div class="modal fade" id="loginModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header p-4 py-2">
                <h4 class="modal-title pl-2 pr-2">নিত্যদিন লগইন</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body ml-4 mr-4">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <input type="hidden" id="backRoute" name="backRoute" value="">
                    <!-- <a href="#" class="btn btn-facebook btn-block mb-2"> <i class="fa fa-facebook-f"></i> &nbsp Sign in
                        with
                        Facebook</a>
                    <a href="#" class="btn btn-google btn-block mb-4"> <i class="fa fa-google"></i> &nbsp Sign in with
                        Google</a> -->
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="control-label">নিবন্ধিত ইমেইল অথবা মোবাইল নাম্বার <small class="text-muted">(ইংরেজিতে)</small></label>
                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" placeholder="নিবন্ধিত ইমেইল অথবা মোবাইল নাম্বার" required autocomplete="email" autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="control-label">পাসওয়ার্ড</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="পাসওয়ার্ড" required autocomplete="current-password">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        @if (Route::has('password.request'))
                            <a class="float-right" href="{{ route('password.request') }}">
                                {{ __('পাসওয়ার্ড ভুলে গেছেন?') }}
                            </a>
                        @endif
                        <label class="float-left custom-control custom-checkbox"> <input type="checkbox"
                                class="custom-control-input" checked="" id="remember"
                                {{ old('remember') ? 'checked' : '' }}>
                            <div class="custom-control-label"> {{ __('লগইন মনে রাখুন') }} </div>
                        </label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block"> লগইন করুন </button>
                    </div>
                </form>
                <hr>
                <p class="text-center mt-4">কোন একাউন্ট নেই? <a class="registerBtn" href="#">নিবন্ধন করুন</a></p>
                <br>
            </div>
        </div>
    </div>
</div>

@push('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            $('#loginModal').on('show.bs.modal', function () {
                $('#backRoute').val(window.location.href);
            });

            $('#loginModal').on('hide.bs.modal', function () {
                $('#backRoute').val('');
            });
        });
    </script>

@endpush