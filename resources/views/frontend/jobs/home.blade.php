@extends('frontend.jobs.index')
@section('title', 'কর্মসংস্থান')
@section('meta_keywords', config('app.name') . 'কর্মসংস্থান')
@section('jobs_content')

<div class="container">
    <section class="padding-bottom-sm">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase">কর্মসংস্থান</h4>
        </div>
    </section>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>
@endpush