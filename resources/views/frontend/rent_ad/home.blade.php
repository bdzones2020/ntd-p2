@extends('frontend.rent_ad.index')
@section('title', 'ভাড়ার বিজ্ঞাপন')
@section('meta_keywords', config('app.name') . 'ভাড়ার বিজ্ঞাপন')
@section('rent_ad_content')

<div class="container">
    <section class="padding-bottom-sm">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase">ভাড়ার বিজ্ঞাপন</h4>
        </div>
    </section>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>
@endpush