@extends('frontend.rent_ad.index')
@section('title', 'আমাদের সম্পর্কে')
@section('meta_keywords', config('app.name') . 'আমাদের সম্পর্কে')

@section('rent_ad_content')

<div class="container">

    <section class="padding-bottom-sm">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase">আমাদের সম্পর্কে</h4>
        </div>
    </section>

</div>
<!-- container end.// -->

@endsection

@push('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>

@endpush