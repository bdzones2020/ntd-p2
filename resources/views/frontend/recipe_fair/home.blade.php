@extends('frontend.recipe_fair.index')
@section('title', 'রান্না মেলা')
@section('meta_keywords', config('app.name') . 'রান্না মেলা')
@section('recipe_fair_content')

<div class="container">
    <section class="padding-bottom-sm">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase">রান্না মেলা</h4>
        </div>
    </section>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>
@endpush