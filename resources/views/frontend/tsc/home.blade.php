@extends('frontend.tsc.index')
@section('title', 'ছাত্র শিক্ষক কেন্দ্র')
@section('meta_keywords', config('app.name') . 'ছাত্র শিক্ষক কেন্দ্র')
@section('tsc_content')

<div class="container">
    <section class="padding-bottom-sm">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase">ছাত্র শিক্ষক কেন্দ্র</h4>
        </div>
    </section>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>
@endpush