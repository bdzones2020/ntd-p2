@extends('frontend.lost_found.index')
@section('title', 'হারানো পাওয়া - ' . $lost_found->title)
@section('meta_keywords', 'হারানো পাওয়া - ' . $lost_found->title)

@section('lost_found_content')

<div class="container">
    <section>
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase mb-0">হারানো পাওয়া</h4>
        </div>
    </section>
    <div class="row row-sm">
        <div class="col-12">
            <div class="card card-product-grid mb-3">
                <div class="card-body p-3">
                    <span class="type-tag {{ $lost_found->occurance_type }}">
                        {{ $lost_found->occurance_type == 'lost' ? 'হারিয়েছে' : 'পাওয়া গেছে' }}ঃ 
                        <span>{{ get_subject($lost_found->subject_type) }}</span>
                    </span>
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 border-right">
                            @if(count($lost_found->lost_found_images) > 0)
                                <div id="imageSlider" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ul class="carousel-indicators">
                                        @foreach($lost_found->lost_found_images as $i => $image)
                                        <li data-target="#imageSlider" data-slide-to="{{ $i }}" class="{{ $i == 0 ? 'active' : '' }}"></li>
                                        @endforeach
                                    </ul>

                                    <!-- The slideshow -->
                                    <div class="carousel-inner">
                                        @foreach($lost_found->lost_found_images as $i => $image)
                                        <div class="carousel-item {{ $i == 0 ? 'active' : '' }}">
                                            <img src="{{ asset('images/lost_founds/'.$image->image_link) }}" alt="image" class="img-fluid" />
                                        </div>
                                        @endforeach
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="carousel-control-prev" href="#imageSlider" data-slide="prev">
                                        <i class="fa fa-angle-left text-secondary fa-2x"></i>
                                    </a>
                                    <a class="carousel-control-next" href="#imageSlider" data-slide="next">
                                        <i class="fa fa-angle-right text-secondary fa-2x"></i>
                                    </a>
                                </div>
                            @else
                                <img class="img-fluid" src="{{ asset('img/900x900.jpg') }}">
                            @endif
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 post-box">
                            <h4 class="mb-0">{{ $lost_found->title }}</h4>
                            <div class="d-block">
                                <small>
                                    <strong>প্রকাশকঃ {{ $lost_found->user ? $lost_found->user->name : '' }}</strong>, {{ date('d-m-Y H:i',strtotime($lost_found->created_at)) }}
                                </small>
                            </div>
                            <hr class="mt-1 mb-1">
                            <div class="row">
                                <div class="col-sm-6 pl-md-3 plr-15">
                                    @if($lost_found->subject_type == 'person')
                                        নামঃ {{ $lost_found->subject_name }} <br>
                                    @elseif($lost_found->subject_type == 'pet')
                                        প্রাণীঃ {{ $lost_found->subject_name }} <br>
                                    @else
                                        বস্তুঃ {{ $lost_found->subject_name }} <br>
                                    @endif

                                    ঠিকানাঃ {{ $lost_found->address }} <br>
                                    স্থানঃ {{ $lost_found->thana->name }}, {{ $lost_found->district->name }}, {{ $lost_found->division->name }} <br>
                                </div>
                                <div class="col-sm-6 text-right text-sm-left plr-15">
                                    ঘটনার তারিখঃ {{ date('d-m-Y', strtotime($lost_found->occurance_date)) }} <br>
                                    আনুমানিক সময়ঃ {{ $lost_found->occurance_time }}
                                </div>
                                <div class="col-12">
                                    
                                </div>
                            </div>
                            <p class="border-top">
                                {!! $lost_found->description !!}
                            </p>
                        </div>
                    </div>
                    <hr>
                    <div class="row mb-3">
                        <div class="col-12 justify-content-between plr-15">
                            <strong class="font-125 font-weight-bolder">সাড়া / প্রতিউত্তর সমূহ {{ count($lost_found->lost_found_responses) > 1 ? '('.count($lost_found->lost_found_responses).')' : '' }}</strong>
                            @auth
                                <button id="addLostFoundResponseBtn" data-id="{{ $lost_found->id }}" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="top">সাড়া / প্রতিউত্তর দিন</button>
                            @else
                                <button class="btn btn-primary loginBtn pull-right" data-toggle="tooltip" data-placement="top">সাড়া / প্রতিউত্তর দিন</button>
                            @endauth
                        </div>
                    </div>
                    @if(!empty($lost_found->lost_found_responses) && count($lost_found->lost_found_responses) > 0)
                        <div class="row">
                            @foreach($lost_found->lost_found_responses as $lost_found_response)
                                <div class="col-12 mb-2 plr-15">
                                    <div class="card">
                                        <div class="card-body p-2 shadow">
                                            <div class="d-block">
                                                <div class="avatar">
                                                    <img src="{{ !empty($lost_found_response->user->avatar) ? asset('images/user/avatar/'.$lost_found_response->user->avatar) : asset('img/user-dummy.png') }}" alt="{{ $lost_found_response->user->name }}" class="img-fluid img-circle">
                                                </div>
                                                <div class="avatar-title">
                                                    {{ $lost_found_response->user->name }}
                                                </div>
                                                <span class="float-right">
                                                    <strong>প্রকাশকাল:</strong> {{ date('d-m-Y H:i',strtotime($lost_found_response->created_at)) }}
                                                </span>
                                            </div>
                                            <p class="pl-5">
                                                {{ $lost_found_response->description }}
                                            </p>
                                            @if(!empty($lost_found_response->lost_found_response_images) && count($lost_found_response->lost_found_response_images) > 0)
                                                <div class="row mt-2">
                                                    @foreach($lost_found_response->lost_found_response_images as $response_image)
                                                        <div class="col-sm-2 col-3 plr-15">
                                                            <img src="{{ !empty($response_image->image_link) ? asset('images/lost_founds/thumbs/'.$response_image->image_link) : asset('img/dummy.jpg') }}" alt="image" class="img-fluid img-thumbnail">
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>
@endpush