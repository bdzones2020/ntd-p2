@extends('frontend.lost_found.index')
@section('title', 'হারানো পাওয়া')
@section('meta_keywords', config('app.name') . 'হারানো পাওয়া')

@section('lost_found_content')

<div class="container">
    <section class="">
        <div class="section-heading heading-line">
            <h4 class="title-section text-uppercase mb-0">হারানো পাওয়া</h4>
        </div>
    </section>
    <div class="row row-sm">
        @if(count($lost_founds) > 0)
            @foreach($lost_founds as $l => $lost_found)
                <div class="col-lg-6 col-md-12">
                    <div class="card card-product-grid mb-3">
                        <div class="card-body p-3">
                            <span class="type-tag {{ $lost_found->occurance_type }}">
                                {{ $lost_found->occurance_type == 'lost' ? 'হারিয়েছে' : 'পাওয়া গেছে' }}ঃ 
                                <span>{{ get_subject($lost_found->subject_type) }}</span>
                            </span>
                            <div class="row">
                                <div class="col-5">
                                    @if(!empty($lost_found->first_image))
                                        <img src="{{ asset('images/lost_founds/thumbs/'.$lost_found->first_image->image_link) }}" alt="image" class="img-fluid" />
                                    @else
                                        <img class="img-fluid" src="{{ asset('img/900x900.jpg') }}">
                                    @endif
                                </div>
                                <div class="col-7">
                                    <h4 class="mb-2">{{ substr($lost_found->title, 0, 25) }}</h4>
                                    <hr class="mt-0 mb-1">
                                    <p>
                                        @if($lost_found->subject_type == 'person')
                                            নামঃ {{ $lost_found->subject_name }} <br>
                                        @elseif($lost_found->subject_type == 'pet')
                                            প্রাণীঃ {{ $lost_found->subject_name }} <br>
                                        @else
                                            বস্তুঃ {{ $lost_found->subject_name }} <br>
                                        @endif
                                        
                                        ঘটনার তারিখঃ {{ date('d-m-Y', strtotime($lost_found->occurance_date)) }} <br>
                                        ঠিকানাঃ {{ $lost_found->address }} <br>
                                        স্থানঃ {{ $lost_found->thana->name }}, {{ $lost_found->district->name }}, {{ $lost_found->division->name }} <br>
                                        <a href="{{ route('lost-found.details',['id' => $lost_found->id]) }}" class="btn btn-sm btn-primary btn-details">বিস্তারিত</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        <div class="col-12">
            {{ $lost_founds->links('pagination::bootstrap-4') }}
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>
@endpush