@push('styles')
    <link href="{{ asset('plugins/tagInputs/tagsinput.css') }}" rel="stylesheet" />
@endpush

<div class="modal fade" id="lostFoundFormModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            
            <form id="lostFoundInputForm" action="{{ route('lost-found.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header p-4 py-2">
                    <h4 class="modal-title">কি হারানো / পাওয়া গেছে তা বিস্তারিত লিখুন</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body ml-4 mr-4 font-110">
                    <div class="row mb-2">
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="control-label"><strong>শিরোনাম</strong></label>
                                <input type="text" name="title" class="form-control" id="title" value="{{ old('title') }}" placeholder="শিরোনাম" autofocus required>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-6 mb-2">
                            <label for=""><strong>ঘটনার ধরন</strong></label>
                        </div>
                        <div class="col-lg-8 col-md-6 mb-2">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="radio" class="custom-control-input occurance_type" id="lost" name="occurance_type" value="lost">
                                <label class="custom-control-label" for="lost">হারানো</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="radio" class="custom-control-input occurance_type" id="found" name="occurance_type" value="found">
                                <label class="custom-control-label" for="found">পাওয়া</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-6 mb-2">
                            <label for=""><strong>বিষয়বস্তুর ধরন</strong></label>
                        </div>
                        <div class="col-lg-8 col-md-6 mb-2">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="radio" class="custom-control-input subject_type" id="person" name="subject_type" value="person">
                                <label class="custom-control-label" for="person">ব্যক্তি</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="radio" class="custom-control-input subject_type" id="pet" name="subject_type" value="pet">
                                <label class="custom-control-label" for="pet">পোষা প্রাণী</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="radio" class="custom-control-input subject_type" id="meterial" name="subject_type" value="meterial">
                                <label class="custom-control-label" for="meterial">বস্তু</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-6 mb-2">
                            <label for="subject_name"><strong>ব্যক্তি/প্রানি/বস্তুর নাম</strong></label>
                        </div>
                        <div class="col-lg-8 col-md-6 mb-2">
                            <input type="text" name="subject_name" class="form-control" id="subject_name" value="{{ old('subject_name') }}" placeholder="ব্যক্তি/প্রানি/বস্তুর নাম" required>
                            @if ($errors->has('subject_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('subject_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
                    @include('frontend.helpers.location_inputs_helper')

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="control-label"><strong>ঠিকানা</strong></label>
                                <input type="text" name="address" class="form-control" id="address" value="{{ old('address') }}" placeholder="ঠিকানা" autofocus required>
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group{{ $errors->has('occurance_date') ? ' has-error' : '' }}">
                                <label for="occurance_date" class="control-label"><strong>ঘটনার তারিখ</strong></label>
                                <input type="date" name="occurance_date" class="form-control" id="occurance_date" value="{{ old('occurance_date') }}" placeholder="ঘটনার তারিখ" required>
                                @if ($errors->has('occurance_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('occurance_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-6">
                            <div class="form-group{{ $errors->has('occurance_time') ? ' has-error' : '' }}">
                                <label for="occurance_time" class="control-label"><strong>আনুমানিক সময়</strong></label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <select name="hour" id="hour" class="form-control px-0 select2" required>
                                            <option value="01">০১ ঘঃ</option>
                                            <option value="02">০২ ঘঃ</option>
                                            <option value="03">০৩ ঘঃ</option>
                                            <option value="04">০৪ ঘঃ</option>
                                            <option value="05">০৫ ঘঃ</option>
                                            <option value="06">০৬ ঘঃ</option>
                                            <option value="07">০৭ ঘঃ</option>
                                            <option value="08">০৮ ঘঃ</option>
                                            <option value="09">০৯ ঘঃ</option>
                                            <option value="10">১০ ঘঃ</option>
                                            <option value="11">১১ ঘঃ</option>
                                            <option value="12">১২ ঘঃ</option>
                                        </select>
                                    </div>
                                    <div class="input-group-prepend">
                                        <select name="minute" id="minute" class="form-control px-0 select2" required>
                                            <option value="00">০০ মিঃ</option>
                                            <option value="05">০৫ মিঃ</option>
                                            <option value="10">১০ মিঃ</option>
                                            <option value="15">১৫ মিঃ</option>
                                            <option value="20">২০ মিঃ</option>
                                            <option value="25">২৫ মিঃ</option>
                                            <option value="30">৩০ মিঃ</option>
                                            <option value="35">৩৫ মিঃ</option>
                                            <option value="40">৪০ মিঃ</option>
                                            <option value="45">৪৫ মিঃ</option>
                                            <option value="50">৫০ মিঃ</option>
                                            <option value="55">৫৫ মিঃ</option>
                                        </select>
                                    </div>
                                    <div class="input-group-append">
                                        <select name="period" id="period" class="select2" required>
                                            <option value="day">দিন</option>
                                            <option value="night">রাত</option>
                                        </select>
                                    </div>
                                </div>

                                @if ($errors->has('occurance_time'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('occurance_time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="images" class="control-label"><strong>ছবি</strong></label>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <div class="custom-file">
                                    <input id="image" type="file" name="images[]" class="custom-file-input" accept=".jpg, .jpeg, .png" multiple="">
                                    <label class="custom-file-label" for="validatedCustomFile">ছবি যোগ করুন ...</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row" id="imageTray">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="description" class="control-label"><strong>বিস্তারিত</strong></label>
                        </div>
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <textarea class="form-control" name="description" id="description" cols="30" rows="10" required>{{ old('description') }}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-12 mb-2">
                            <label for="tags"><strong>ট্যাগ</strong></label>
                        </div>
                        <div class="col-lg-8 col-md-6 mb-2">
                            <input class="form-control" type="text" id="tags" name="tags" data-role="tagsinput" value="{{ old('tags') }}" placeholder="ট্যাগ" required>
                            @if ($errors->has('tags'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tags') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-12 mb-2">
                            <label for="duration"><strong>কত দিন পরে আর্কাইভ / সংরক্ষিত হবে?</strong></label>
                        </div>
                        <div class="col-lg-8 col-md-6 mb-2">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="radio" class="custom-control-input duration" id="7_days" name="duration" value="7_days">
                                <label class="custom-control-label" for="7_days">৭ দিন</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="radio" class="custom-control-input duration" id="15_days" name="duration" value="15_days">
                                <label class="custom-control-label" for="15_days">১৫ দিন</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="radio" class="custom-control-input duration" id="30_days" name="duration" value="30_days">
                                <label class="custom-control-label" for="30_days">৩০ দিন</label>
                            </div>
                            @if ($errors->has('subject_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('subject_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer mr-4">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">বাতিল করুন</button>
                    <button type="submit" class="btn btn-outline-info">প্রকাশ / পোষ্ট করুন</button>
                </div>
            </form>

        </div>
    </div>
</div>

@push('scripts')
    <script src="{{ asset('plugins/tagInputs/tagsinput.js') }}"></script>
    <script type="text/javascript">
        window.onload = function(){
            var caption = "<input type='text' class='form-control' name='caption[]' value='' placeholder='ছবির সারসংক্ষেপ'>";
            //Check File API support
            if(window.File && window.FileList && window.FileReader)
            {
                var filesInput = document.getElementById("image");
                filesInput.addEventListener("change", function(event){
                    var files = event.target.files; //FileList object
                    var output = document.getElementById("imageTray");
                    for(var i = 0; i< files.length; i++)
                    {
                        var file = files[i];
                        //Only pics
                        if(!file.type.match('image'))
                            continue;
                        var picReader = new FileReader();
                        picReader.addEventListener("load",function(event){
                            var picFile = event.target;
                            var div = document.createElement("div");
                            div.classList.add("col-md-4");
                            div.classList.add("mb-3");
                            div.innerHTML = "<button type='button' class='btn btn-sm btn-danger btn-delete-image' onclick='this.parentElement.remove();'>X</button><img class='img-fluid' src='" + picFile.result + "'" + "title='" + picFile.name + "'/><input type='hidden' name='uploadedImages[]' value='true'>";
                            output.insertBefore(div,null);
                        });
                        //Read the image
                        picReader.readAsDataURL(file);
                    }
                });
            }
            else
            {
                console.log("Your browser does not support File API");
            }
        }
        var minDate = {!! json_encode(date('Y-m-d',strtotime('now -2 week'))) !!};
        var maxDate = {!! json_encode(date('Y-m-d')) !!};
        document.getElementById("occurance_date").min = minDate;
        document.getElementById("occurance_date").max = maxDate;

        $(document).ready(function(){
            $('.occurance_type, .subject_type, .duration').prop('checked',false);
            $("#addLostFoundBtn").click(function(){
                $("#lostFoundFormModal").modal({backdrop: "static"});
            });
        });
    </script>
@endpush