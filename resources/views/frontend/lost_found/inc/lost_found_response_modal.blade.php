<div class="modal fade" id="lostFoundReponseModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            
            <form action="{{ route('lost-found.store-response') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" class="lost_found_id" name="lost_found_id">
                <div class="modal-header p-4 py-2">
                    <h4 class="modal-title">সাড়া / প্রতিউত্তর দিন</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body ml-4 mr-4 font-110">
                                        
                    @include('frontend.helpers.location_inputs_helper')

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="control-label"><strong>ঠিকানা</strong></label>
                                <input type="text" name="address" class="form-control" id="address" value="{{ old('address') }}" placeholder="ঠিকানা" autofocus required>
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <label for="responseImage" class="control-label"><strong>ছবি</strong></label>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <div class="custom-file">
                                    <input id="responseImage" type="file" name="responseImages[]" class="custom-file-input" accept=".jpg, .jpeg, .png" multiple="">
                                    <label class="custom-file-label" for="validatedCustomFile">ছবি যোগ করুন ...</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row" id="responseImageTray">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="description" class="control-label"><strong>বিস্তারিত</strong></label>
                        </div>
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <textarea class="form-control" name="description" id="description" cols="30" rows="10" required>{{ old('description') }}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer mr-4">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">বাতিল করুন</button>
                    <button type="submit" class="btn btn-outline-info">প্রকাশ / পোষ্ট করুন</button>
                </div>
            </form>

        </div>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        window.onload = function(){
            var caption = "<input type='text' class='form-control' name='caption[]' value='' placeholder='ছবির সারসংক্ষেপ'>";
            //Check File API support
            if(window.File && window.FileList && window.FileReader)
            {
                var filesInput = document.getElementById("responseImage");
                filesInput.addEventListener("change", function(event){
                    var files = event.target.files; //FileList object
                    var output = document.getElementById("responseImageTray");
                    for(var i = 0; i< files.length; i++)
                    {
                        var file = files[i];
                        //Only pics
                        if(!file.type.match('image'))
                            continue;
                        var picReader = new FileReader();
                        picReader.addEventListener("load",function(event){
                            var picFile = event.target;
                            var div = document.createElement("div");
                            div.classList.add("col-md-4");
                            div.classList.add("mb-3");
                            div.innerHTML = "<button type='button' class='btn btn-sm btn-danger btn-delete-image' onclick='this.parentElement.remove();'>X</button><img class='img-fluid' src='" + picFile.result + "'" + "title='" + picFile.name + "'/><input type='hidden' name='uploadedImages[]' value='true'>";
                            output.insertBefore(div,null);
                        });
                        //Read the image
                        picReader.readAsDataURL(file);
                    }
                });
            }
            else
            {
                console.log("Your browser does not support File API");
            }
        }

        $(document).ready(function(){
            $("#addLostFoundResponseBtn").click(function(){
                $('.lost_found_id').val('');
                $('.lost_found_id').val($(this).data('id'));
                $("#lostFoundReponseModal").modal({backdrop: "static"});
            });
        });
    </script>
@endpush