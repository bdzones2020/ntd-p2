@extends('layouts.app')

@section('topbar_links')
    <a href="{{ route('lost-found./') }}" class="btn-icon px-1 py-1" title="হারানো পাওয়া" data-toggle="tooltip" data-placement="top">
        <i class="fa fa-home"></i> <span>হারানো পাওয়া</span>
    </a>
    <a href="{{ route('lost-found.terms-&-conditions') }}" class="btn-icon px-1 py-1" title="শর্তাবলী" data-toggle="tooltip" data-placement="top">
        <i class="fa fa-list"></i> <span>শর্তাবলী</span>
    </a>
    <a href="{{ route('lost-found.faq') }}" class="btn-icon px-1 py-1" title="জিজ্ঞাসা" data-toggle="tooltip" data-placement="top">
        <i class="fa fa-question-circle-o"></i> <span>জিজ্ঞাসা</span>
    </a>
    @auth
        <a class="btn-icon px-1 py-1" title="প্রকাশ করুন" data-toggle="tooltip" data-placement="top" id="addLostFoundBtn">
            <i class="fa fa-plus"></i> <span>প্রকাশ করুন</span>
        </a>
    @else
        <a class="btn-icon px-1 py-1 loginBtn" title="প্রকাশ করুন" data-toggle="tooltip" data-placement="top">
            <i class="fa fa-plus"></i> <span>প্রকাশ করুন</span>
        </a>
    @endauth
    <!-- <span class="dropdown">
        <a class="btn-icon px-1 py-1 dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-plus"></i> <span>প্রকাশ করুন</span>
        </a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="#" id="addLost">হারানো গেছে</a>
            <a class="dropdown-item" href="#">পাওয়া গেছে</a>
            <a class="dropdown-item" href="#">আমার প্রকাশগুলি</a>
        </div>
    </span> -->

@endsection

@section('content')
    @yield('lost_found_content')

    @include('frontend.lost_found.inc.lost_found_input_modal')
    @include('frontend.lost_found.inc.lost_found_response_modal')
@endsection