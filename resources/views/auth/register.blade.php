@extends('frontend.web.index')

@section('web_content')
    <section class="section-content">

        <div class="card mx-auto" style="max-width:480px; margin-top:40px;">
            <article class="card-body">
                <h4 class="card-title mb-4 text-center border-bottom">নিত্যদিন নিবন্ধন</h4>
                <form id="register" class="form-horizontal" method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="control-label">নিবন্ধনকারীর নাম</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" placeholder="নিবন্ধনকারীর নাম" autofocus required>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="control-label">
                            ইমেইল
                            <small class="text-blue"> (আপনি ইমেইল ছাড়াও নিবন্ধন করতে পারবেন। সেক্ষেত্রে আপনার ইমেইল বিষয়ক সুবিধাগুলি নিষ্ক্রিয় থাকবে।)</small>
                        </label>
                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="নিবন্ধিত ইমেইল" required autocomplete="email">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                        <label>মোবাইল নাম্বার <small class="text-muted">(ইংরেজিতে)</small> <span class="text-blue">(মোট ১১ সংখ্যা 01 সহ)</span> </label>
                        <input type="tel" name="mobile_number" class="form-control quantity_class" id="mobile_number" value="{{ old('mobile_number') }}" minlength="11" maxlength="11" placeholder="01XXXXXXXXX" required="">
                        <div id="mobile_number_alert" class="text-danger">Invalid Contact Number</div>
                        @if ($errors->has('mobile_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('mobile_number') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-md-6">
                            <label for="password" class="control-label">পাসওয়ার্ড</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="পাসওয়ার্ড" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-md-6">
                            <label for="password_confirmation" class="control-label">পাসওয়ার্ড নিশ্চিত করুন</label>
                            <input id="password_confirmation" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" placeholder="পাসওয়ার্ড" required>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" checked="" required="">
                            <div class="custom-control-label"> <a href="#">শর্তাবলীত্বে </a> আমার সম্মতি আছে </div>
                        </label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block"> নিবন্ধন করুন </button>
                    </div>
                </form>
            </article>
        </div>
        <p class="text-center mt-4">একাউন্ট আছে? <a href="{{ route('login') }}">লগইন করুন</a></p>
        <br><br>


    </section>
@endsection

@push('scripts')
<script>
    $(document).ready(function(){

        $('#mobile_number_alert').hide();

        $('#mobile_number').change(function(){
            var checkNumber = $(this).val();
            if(checkNumber.length !== 11)
            {
                $('#mobile_number_alert').show();
            }else if(checkNumber.substr(0, 2) !== '01')
            {
                $('#mobile_number_alert').show();
            }else{
                $('#mobile_number_alert').hide();
            }
        });

    });
</script>
@endpush