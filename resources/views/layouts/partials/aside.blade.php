<div id="aside-wrapper" class="theme-bg">
    <ul class="aside-nav">
        <li class="list-group-item">
            <a href="{{ route('about-us') }}">আমাদের সম্পর্কে</a>
        </li>
        <li class="list-group-item">
            <a href="{{ route('terms-&-conditions') }}">শর্তাবলী</a>
        </li>
    </ul>
</div>