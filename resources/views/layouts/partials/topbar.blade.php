<div class="toparea theme-bg text-secondary animated">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-lg-10 col-md-9 col-sm-9 col-10 toparea-btn-box">

                @yield('topbar_links')

            </div>
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-2">
                
                @auth
                    <div class="dropdown pull-right">
                        <a href="javascript:void(0);" id="topareaProfile" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user-circle-o"></i>
                            <span class="username">{{ Auth::user()->name }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="topareaProfile">

                            @if(Auth::user()->type == 'system')

                            <a href="{{ route('system./') }}" class="dropdown-item text-dark"><i class="fa fa-dashboard"></i> System</a>

                            @endif

                            <a href="" class="dropdown-item"><i class="fa fa-user"></i> ব্যক্তিগত তথ্য</a>

                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item"><i class="fa fa-sign-out"></i> লগআউট</a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </div>
                    </div>
                @else
                    <span class="pull-right"><a href="#" class="loginBtn"><i class="fa fa-sign-in"></i> <span>লগইন</span></a></span>
                    <span class="pull-right mr-2"><a class="registerBtn" href="#"><i class="fa fa-user-plus"></i> <span>নিবন্ধন</span></a></span>
                @endauth

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="locationModal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="header_submit" method="get" accept-charset="utf-8">
                <div class="modal-header">
                    <h6 class="modal-title">Set Location</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                        
                    @include('frontend.helpers.search_location_helper')
                    
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

@include('frontend.helpers.login_modal')

@include('frontend.helpers.register_modal')

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.loginBtn').click(function(e){
                e.preventDefault();
                $("#registerModal").modal('hide');
                $("#loginModal").modal({backdrop: "static"});
            });
            $('.registerBtn').click(function(e){
                e.preventDefault();
                $("#loginModal").modal('hide');
                $("#registerModal").modal({backdrop: "static"});
            });
        });
    </script>
@endpush