@php $route = Route::current() @endphp
<nav class="navbar navbar-expand-md fixed-top bg-white">
    <div class="container">
        <a class="navbar-brand" style="margin-left:0px;" href="{{ route('/') }}">
            <img class="logo" src="{{ asset('img/nityodin_logo.png') }}">
            {{ config('app.name') }}
        </a>

        @if($route->getPrefix())
            <select id="page-toggler">
                <option value="sale-info" {{ $route->getPrefix() == '/sale-info' ? 'selected' : '' }}>বিক্রয় তথ্য</option>
                <option value="lost-found" {{ $route->getPrefix() == '/lost-found' ? 'selected' : '' }}>হারানো পাওয়া</option>
                <option value="human-resource" {{ $route->getPrefix() == '/human-resource' ? 'selected' : '' }}>মানবসম্পদ</option>
                <option value="tsc" {{ $route->getPrefix() == '/tsc' ? 'selected' : '' }}>ছাত্র শিক্ষক কেন্দ্র</option>
                <option value="exchange" {{ $route->getPrefix() == '/exchange' ? 'selected' : '' }}>অদল বদল</option>
                <option value="jobs" {{ $route->getPrefix() == '/jobs' ? 'selected' : '' }}>কর্মসংস্থান</option>
                <option value="humanity-service" {{ $route->getPrefix() == '/humanity-service' ? 'selected' : '' }}>মানবিক সেবা</option>
                <option value="blog" {{ $route->getPrefix() == '/blog' ? 'selected' : '' }}>মুক্তমঞ্চ</option>
                <option value="recipe-fair" {{ $route->getPrefix() == '/recipe-fair' ? 'selected' : '' }}>রান্না মেলা</option>
                <option value="rent-ad" {{ $route->getPrefix() == '/rent-ad' ? 'selected' : '' }}>ভাড়ার বিজ্ঞাপন</option>
                <option value="information" {{ $route->getPrefix() == '/information' ? 'selected' : '' }}>প্রয়োজনীয় তথ্য</option>
            </select>
        @endif

        <!-- <span class="">
            <i class="fa fa-map-marker"></i>
            <span class="badge badge-pill badge-warning">Warning</span>
        </span> -->
        <button class="btn btn-sm btn-light location-btn" href="#locationModal" data-toggle="modal" id="getLocation" type="button">
            <i class="fa fa-map-marker text-primary"></i> <span id="locationNav">অবস্থান</span>
        </button>
        <!-- <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('terms-&-conditions') }}">শর্তাবলী</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('about-us') }}">আমাদের সম্পর্কে</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('faq') }}">জিজ্ঞাসা</a>
                </li>
            </ul>
        </div> -->
    </div>
</nav>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            var AppUrl = {!! json_encode(url('/')) !!}
            $('#page-toggler').change(function(){
                window.location = AppUrl+'/'+$(this).val();
            });
        });
    </script>
@endpush