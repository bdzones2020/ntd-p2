@php $route = Route::current() @endphp
@php $groups = menus() @endphp
<div id="sidebar-wrapper" class="theme-bg">
    <ul class="sidebar-nav theme-bg">
        <li class="list-group-item">
            <a href="" class="">
                Services
            </a>
        </li>

        @if(count($groups) > 0)
            @foreach($groups as $g => $group)
                
                @if(count($group->categories) > 0)

                    <li class="list-group-item">
                        <a class="category-list-btn" href="#group_{{ $group->id }}" data-id="{{ $group->id }}">
                            {{ $group->name }} <i class="fa fa-angle-right pull-right"></i>
                        </a>
                        <ul class="list-group collapse category-list" id="categories_{{ $group->id }}">

                            @foreach($group->categories as $c => $category)
                        
                                @if(count($category->subcategories) > 0)

                                    <li class="list-group-item">
                                        <a class="subcategory-list-btn" href="#category_{{ $category->id }}" data-id="{{ $category->id }}">
                                            {{ $category->name }} <i class="fa fa-angle-right pull-right"></i>
                                        </a>
                                        <ul class="list-group collapse subcategory-list" id="subcategories_{{ $category->id }}">

                                            @foreach($category->subcategories as $s => $subcategory)

                                                <li class="list-group-item">
                                                    <a href="{{ route('products',['groupSlug' => $group->slug, 'categorySlug' => $category->slug, 'subcategorySlug' => $subcategory->slug]) }}" class="">
                                                        {{ $subcategory->name }}
                                                    </a>
                                                </li>

                                            @endforeach

                                        </ul>
                                    </li>

                                @else

                                    <li class="list-group-item">
                                        <a href="{{ route('products',['groupSlug' => $group->slug, 'categorySlug' => $category->slug]) }}" class="">
                                            {{ $category->name }}
                                        </a>
                                    </li>

                                @endif

                            @endforeach

                        </ul>
                    </li>

                @else

                    <li class="list-group-item">
                        <a href="{{ route('products',['groupSlug' => $group->slug]) }}" class="">
                            {{ $group->name }}
                        </a>
                    </li>

                @endif

            @endforeach
        @endif

    </ul>
</div>