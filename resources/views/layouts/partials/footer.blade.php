<footer class="section-footer">
    <div class="container">
        <section class="footer-bottom text-center">
            <p class="">সর্বস্বত্বাধিকারী &copy {{ config('app.name') }}</p>
        </section>
    </div>
</footer>