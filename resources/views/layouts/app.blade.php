@php $appInfo = appInfo() @endphp
<!DOCTYPE html>
<html dir="ltr" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="{{ !empty($appInfo->name) ? $appInfo->name : config('app.name') }}, Local Business Comminication & Management System, @yield('meta_keywords')">
        <meta name="description" content="{{ !empty($appInfo->name) ? $appInfo->name : config('app.name') }}, Local Business Comminication & Management System, @yield('meta_description')">
        <meta name="author" content="mosharof.bdzones@gmail.com">

        <!-- favicon -->
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/nityodin_logo.png') }}">

        <!-- title -->
        <title>{{ config('app.name') }} @yield('title')</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Styles -->
        <link href="{{ asset('web/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- select2 -->
        <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet" />

        <!-- Template Main CSS File -->
        <link href="{{ asset('web/css/animate.css') }}" rel="stylesheet" />
        <link href="{{ asset('web/css/ui.css') }}" rel="stylesheet" />
        <link href="{{ asset('web/css/responsive.css') }}" rel="stylesheet" />
        <link href="{{ asset('web/css/layouts.css') }}" rel="stylesheet" />
        <link href="{{ asset('web/css/custom.css') }}" rel="stylesheet" />

        <!-- font awesome -->
        <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400&display=swap" rel="stylesheet"> 

        @stack('styles')

    </head>

    <body>
        
        <div id="app">

            @include('layouts.partials.header')

            @include('layouts.partials.topbar')

            <div id="wrapper">

                @include('layouts.partials.aside')

                <!-- Page Content -->
                <div id="homepage-content-wrapper">

                    <div class="page-content">

                        @include('layouts.partials.message')
                        
                        @yield('content')

                    </div>

                    @include('layouts.partials.footer')

                </div>
                <!-- /#page-content-wrapper -->

            </div>

        </div>

        <!-- Scripts -->
        <script src="{{ asset('web/js/jquery-3.5.1.min.js') }}"></script>
        <script src="{{ asset('web/js/jquery-migrate-3.0.0.min.js') }}"></script>
        <script src="{{ asset('web/js/popper.min.js') }}"></script>
        <script src="{{ asset('web/js/bootstrap-4.5.2.min.js') }}"></script>
        <script src="{{ asset('plugins/select2/select2.min.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                // remove char from numaric type text field
                $('.quantity_class').attr('onkeyup',"this.value=this.value.replace(/[^0-9:]/g,'');");

                // $("#menu-toggle").click(function(e) {
                //     e.preventDefault();
                //     $("#aside-wrapper").removeClass("toggled");
                //     $("#wrapper").toggleClass("toggled");
                //     $('#navbarSupportedContent').collapse('hide');
                // });
                $("#aside-toggler").click(function(e) {
                    e.preventDefault();
                    $("#wrapper").removeClass('toggled');
                    $("#aside-wrapper").toggleClass("toggled");
                });
                $('#navbar-toggler').click(function(){
                    $("#wrapper").removeClass('toggled');
                    $('#navbarSupportedContent').collapse('toggle');
                });
                $('#page-content-wrapper, .page-content, .toparea, .toparea .dropdown a').click(function(){
                    $("#wrapper").removeClass("toggled");
                    $("#aside-wrapper").removeClass("toggled");
                    $('#navbarSupportedContent').collapse('hide');
                });
                $('.message-box').fadeOut(3000);
            });
        </script>

        @stack('scripts')

    </body>
</html>