<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLostFoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ntd_lost_found')->create('lost_founds', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unsigned()->index()->nullable();
            $table->string('lost_found_code')->nullable()->unique();
            $table->string('title')->nullable();
            $table->enum('occurance_type',['lost','found'])->nullable();
            $table->date('occurance_date')->nullable();
            $table->string('occurance_time')->nullable();
            $table->enum('subject_type',['person','pet','meterial'])->nullable();
            $table->string('subject_name')->nullable();
            $table->string('tags')->nullable();
            $table->foreignId('division_id')->unsigned()->index()->nullable();
            $table->foreignId('district_id')->unsigned()->index()->nullable();
            $table->foreignId('thana_id')->unsigned()->index()->nullable();
            $table->string('address')->nullable();
            $table->text('description')->nullable();
            $table->string('duration')->nullable();
            $table->enum('status',['successful','pending','failed'])->nullable();
            $table->boolean('is_archived')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ntd_lost_found')->dropIfExists('lost_founds');
    }
}
