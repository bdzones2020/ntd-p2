<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLostFoundResponseImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ntd_lost_found')->create('lost_found_response_images', function (Blueprint $table) {
            $table->id();
            $table->foreignId('lost_found_id')->unsigned()->index()->nullable();
            $table->foreignId('lost_found_response_id')->unsigned()->index()->nullable();
            $table->string('image_link')->nullable();
            $table->string('caption')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ntd_lost_found')->dropIfExists('lost_found_response_images');
    }
}
