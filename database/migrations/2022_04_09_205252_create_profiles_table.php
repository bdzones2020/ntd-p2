<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->string('full_name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('spouse_name')->nullable();
            $table->date('dob')->nullable();
            $table->string('gender', 10)->nullable();
            $table->string('religion', 30)->nullable();
            $table->string('photo')->nullable();
            $table->bigInteger('present_division_id')->nullable();
            $table->bigInteger('present_district_id')->nullable();
            $table->bigInteger('present_thana_id')->nullable();
            $table->integer('present_postal_code')->nullable();
            $table->string('present_address')->nullable();
            $table->bigInteger('permanent_division_id')->nullable();
            $table->bigInteger('permanent_district_id')->nullable();
            $table->bigInteger('permanent_thana_id')->nullable();
            $table->integer('permanent_postal_code')->nullable();
            $table->string('permanent_address')->nullable();
            $table->string('personal_contact_number')->unique()->nullable();
            $table->string('personal_contact_email')->unique()->nullable();
            $table->string('emergency_contact_person')->nullable();
            $table->string('emergency_contact_relation')->nullable();
            $table->string('emergency_contact_number')->nullable();
            $table->double('profile_score', 5, 2)->nullable();
            $table->string('referral_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
